#include <iostream>
#include <vtkh/filters/LagrangianField.hpp>
#include <vtkh/filters/GhostStripper.hpp>
#include <vtkh/vtkh.hpp>
#include <vtkh/Error.hpp>
#include <vtkm/filter/Gradient.h>
#include <vtkm/Types.h>
#include <vtkm/cont/DataSetBuilderRectilinear.h>
#include <vtkm/cont/DataSetBuilderExplicit.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/ParticleAdvection.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/particleadvection/GridEvaluators.h>
#include <vtkm/worklet/particleadvection/Integrators.h>
#include <vtkm/worklet/particleadvection/Particles.h>
#include <vtkm/cont/Algorithm.h>
#include <vtkm/cont/ArrayHandleCast.h>
#include <vtkm/cont/Invoker.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/Magnitude.h>
#include <math.h>
#include <cmath>

#define PI 3.14159265

static vtkm::Id cycle = 0;
static vtkm::cont::ArrayHandle<vtkm::Particle> SeedArray;
static vtkm::cont::ArrayHandle<vtkm::Particle> SeedArrayOriginal;
static vtkm::cont::ArrayHandle<vtkm::Id> SeedValidity;
// CMWC working parts
#define CMWC_CYCLE 4096   // as George Marsaglia recommends
#define CMWC_C_MAX 809430660  // as George Marsaglia recommends

struct cmwc_state {
  uint32_t Q[CMWC_CYCLE];
  uint32_t c; // must be limited with CMWC_C_MAX
  unsigned i;
};

// Make 32 bit random number (some systems use 16 bit RAND_MAX [Visual C 2012 uses 15 bits!])
uint32_t rand32(void)
{
  uint32_t result = rand();
  return result << 16 | rand();
}

// Init the state with seed
void initCMWC(struct cmwc_state *state, unsigned int seed)
{
  srand(seed);
  for (int i = 0; i < CMWC_CYCLE; i++)
    state->Q[i] = rand32();
  do
    state->c = rand32();
  while (state->c >= CMWC_C_MAX);
  state->i = CMWC_CYCLE - 1;
}

// CMWC engine
uint32_t randCMWC(struct cmwc_state *state)  //EDITED parameter *state was missing
{
  uint64_t const a = 18782; // as Marsaglia recommends
  uint32_t const m = 0xfffffffe;  // as Marsaglia recommends
  uint64_t t;
  uint32_t x;

  state->i = (state->i + 1) & (CMWC_CYCLE - 1);
  t = a * state->Q[state->i] + state->c;
  /* Let c = t / 0xffffffff, x = t mod 0xffffffff */
  state->c = t >> 32;
  x = t + state->c;
  if (x < state->c) {
    x++;
    state->c++;
  }
  return state->Q[state->i] = m - x;
}

namespace vtkh
{

namespace worklets
{
  class VectorEntropy : public vtkm::worklet::WorkletMapField
  {
    public: 
      typedef void ControlSignature(FieldOut, WholeArrayIn);
      typedef void ExecutionSignature(_1, _2, WorkIndex);
      
      VTKM_CONT VectorEntropy(int t, int p, int x, int y, int z)
      {
        dim_theta = t;
        dim_phi = p;
        dims[0] = x;
        dims[1] = y;
        dims[2] = z;
      }
      
      template<typename EntropyType, typename VelocityPortal>
      VTKM_EXEC void operator()(EntropyType &vectorEntropy, const VelocityPortal &velocityArray, const vtkm::Id &index) const
      {
        int num_bins = dim_theta * dim_phi;
        int bins[num_bins] = {0};
        int idx[3];
        idx[0] = index % (dims[0]);
        idx[1] = (index / (dims[0])) % (dims[1]);
        idx[2] = index / ((dims[0]) * (dims[1]));  
        int N = 5;
        int offset = (N-1)/2;            
        int num_samples = ((offset*2)+1) * ((offset*2)+1) * ((offset*2)+1); 
   
        int count = 0;
            
        vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>> sample_vec;
        sample_vec.Allocate(num_samples);
        auto sample_vec_portal = sample_vec.GetPortalControl();
        // Neighborhood sampling code //
        { 
        if(dims[0] > N && dims[1] > N && dims[2] > N) 
          {
          for(int r = idx[2] - offset; r <= idx[2] + offset; r++)
          {
            for(int q = idx[1] - offset; q <= idx[1] + offset; q++)
            {   
              for(int p = idx[0] - offset; p <= idx[0] + offset; p++) // x grows fastest
              {   
                int x_i, y_i, z_i;
                if(p < 0)
                  x_i = -1*p;
                else if(p > (dims[0] -1))    
                 x_i = (dims[0] - 1) - (p - (dims[0] - 1));
                else
                  x_i = p;
                
                if(q < 0)
                  y_i = -1*q;
                else if(q > (dims[1] -1))    
                  y_i = (dims[1] - 1) - (q - (dims[1] - 1));
                else
                  y_i = q;
               
                if(r < 0)
                  z_i = -1*r;
                else if(r > (dims[2] -1))        
                  z_i = (dims[2] - 1) - (r - (dims[2] - 1));
                else
                  z_i = r;
              
                int pt_id = z_i*dims[1]*dims[0] + y_i*dims[0] + x_i;
          
                if(pt_id < dims[0]*dims[1]*dims[2] && pt_id >= 0)
                {
                  auto v = velocityArray.Get(pt_id);
                  sample_vec_portal.Set(count, vtkm::Vec<vtkm::Float64,3>(v[0],v[1],v[2]));
                  count++;  
                }
              }
            }   
          }
        }              
      } // End of finding neighborhood samples //
      { // Perform binning of each sample //
        double theta_range = 360.0/dim_theta;  
        double phi_range = 180.0/dim_phi;  
              
        for(int n = 0; n < count; n++)
        {
          auto sample = sample_vec_portal.Get(n);
          double radius, theta, phi;
          radius = sqrt((sample[0]*sample[0]) + (sample[1]*sample[1]) + (sample[2]*sample[2]));
          if(radius > 0)
          {
            theta = (atan2(sample[1],sample[0]))*(180/PI);
            phi = (acos(sample[2]/radius))*(180/PI);
            if(theta < 0)
              theta = 360 + theta; // theta value is negative, i.e., -90 is the same as 270 
          }
          else
          {
            theta = 0;
            phi = 0;
          }
        
          if(theta == 360)
            theta = 0;
          if(phi == 180)
            phi = 0;
                 
          int t_index = theta/theta_range;
          int p_index = phi/phi_range;
          int bin_index = (t_index * dim_phi) + p_index;
          if(bin_index < dim_theta*dim_phi && bin_index >= 0)
          {
            bins[bin_index]++;
          }
        }
      } // End of binning process //
      { // Compute the entropy // 
        double probability[num_bins];
        int total_sum = 0;
        for(int n = 0; n < num_bins; n++)
        {
          total_sum += bins[n];
        }
            
        for(int n = 0; n < num_bins; n++)
        {
          probability[n] = bins[n]/(total_sum*1.0);
        }
        
        vtkm::Float64 entropy = 0.0;
            
        for(int n = 0; n < num_bins; n++)
        {
          if(probability[n] > 0.0)
            entropy -= (probability[n]*log2(probability[n]));
        }
        vectorEntropy = entropy;      
      } // End compute entropy //
    }

    private:
      int dim_theta, dim_phi;
      int dims[3];
  };
  
  class PointFieldToCellAverageField : public vtkm::worklet::WorkletMapField
  {
    public: 
      typedef void ControlSignature(FieldOut, WholeArrayIn);
      typedef void ExecutionSignature(_1, _2, WorkIndex);
      
      VTKM_CONT PointFieldToCellAverageField(int xdim, int ydim, int zdim)
      {
        dims[0] = xdim;
        dims[1] = ydim;
        dims[2] = zdim;
      }
      
      template<typename CellValue, typename FeaturePortal>
      VTKM_EXEC void operator()(CellValue &cell, const FeaturePortal &feature, const vtkm::Id &index) const
      {
        int idx[3];
        idx[0] = index % (dims[0]-1);
        idx[1] = (index/(dims[0]-1)) % (dims[1]-1);
        idx[2] = index / ((dims[0]-1) * (dims[1]-1));
        double sample_sum = 0;
        sample_sum += feature.Get(idx[2]*dims[0]*dims[1] + idx[1]*dims[0] + idx[0]);
        sample_sum += feature.Get(idx[2]*dims[0]*dims[1] + idx[1]*dims[0] + (idx[0]+1));
        sample_sum += feature.Get(idx[2]*dims[0]*dims[1] + (idx[1]+1)*dims[0] + idx[0]);
        sample_sum += feature.Get(idx[2]*dims[0]*dims[1] + (idx[1]+1)*dims[0] + (idx[0]+1));
        sample_sum += feature.Get((idx[2]+1)*dims[0]*dims[1] + idx[1]*dims[0] + idx[0]);
        sample_sum += feature.Get((idx[2]+1)*dims[0]*dims[1] + idx[1]*dims[0] + (idx[0]+1));
        sample_sum += feature.Get((idx[2]+1)*dims[0]*dims[1] + (idx[1]+1)*dims[0] + idx[0]);
        sample_sum += feature.Get((idx[2]+1)*dims[0]*dims[1] + (idx[1]+1)*dims[0] + (idx[0]+1));
        double average_sum = sample_sum/8.0;
        cell = average_sum;
      }

    private:
      int dims[3];
  };

  class RandomSeedInCell : public vtkm::worklet::WorkletMapField
  {
    public: 
      typedef void ControlSignature(FieldIn, FieldIn, FieldOut);
      typedef void ExecutionSignature(_1, _2, _3);
      
      VTKM_CONT RandomSeedInCell(std::vector<vtkm::Float64> x, std::vector<vtkm::Float64> y, std::vector<vtkm::Float64> z, int xdim, int ydim, int zdim)
      {
        xC = x;
        yC = y;
        zC = z;
  
        dims[0] = xdim;
        dims[1] = ydim;
        dims[2] = zdim;
      }
      
      template<typename CellId, typename RandomPortal, typename SeedParticle>
      VTKM_EXEC void operator()(const CellId &index, const RandomPortal &random, SeedParticle &seed) const
      {
        int idx[3];
        idx[0] = index % (dims[0]-1);
        idx[1] = (index/(dims[0]-1)) % (dims[1]-1);
        idx[2] = index / ((dims[0]-1) * (dims[1]-1));
        
        double bbox[6];
        bbox[0] = xC[idx[0]];
        bbox[1] = xC[idx[0]+1];
        bbox[2] = yC[idx[1]];
        bbox[3] = yC[idx[1]+1];
        bbox[4] = zC[idx[2]];
        bbox[5] = zC[idx[2]+1];
        
        double rangex = bbox[1] - bbox[0];
        double rangey = bbox[3] - bbox[2];
        double rangez = bbox[5] - bbox[4];

        int rx = rangex * 10000; 
        int ry = rangey * 10000;
        int rz = rangez * 10000;
    
//        srand(time(NULL)); 

        double x,y,z; 
        x = (random[0]%rx)/10000.0 + bbox[0];
        y = (random[1]%ry)/10000.0 + bbox[2];
        z = (random[2]%rz)/10000.0 + bbox[4];

        seed.Pos[0] = x; 
        seed.Pos[1] = y; 
        seed.Pos[2] = z; 
      }

    private:
      std::vector<vtkm::Float64> xC, yC, zC;
      int dims[3];
  };

  class ExponentWorklet : public vtkm::worklet::WorkletMapField
  {
    public: 
      typedef void ControlSignature(FieldInOut);
      typedef void ExecutionSignature(_1);
      
      VTKM_CONT ExponentWorklet(double e)
      {
        exponent = e;
      }
      
      template<typename FeaturePortal>
      VTKM_EXEC void operator()(FeaturePortal &value) const
      {
        value = pow(value, exponent); 
      }

    private:
      double exponent;
  };

  class AbsoluteValueWorklet : public vtkm::worklet::WorkletMapField
  {
    public: 
      typedef void ControlSignature(FieldInOut);
      typedef void ExecutionSignature(_1);
      
      VTKM_CONT AbsoluteValueWorklet()
      {}
      
      template<typename FeaturePortal>
      VTKM_EXEC void operator()(FeaturePortal &value) const
      {
        value = abs(value); 
      }
  };

  class MinimumValueWorklet : public vtkm::worklet::WorkletMapField
  {
    public: 
      typedef void ControlSignature(FieldInOut);
      typedef void ExecutionSignature(_1);
      
      VTKM_CONT MinimumValueWorklet(double v)
      {
        min = v;
      }
      
      template<typename FeaturePortal>
      VTKM_EXEC void operator()(FeaturePortal &value) const
      {
        if(value < min)
          value = min;
        else
          value = value; 
      }

    private:
      double min;
  };

  class SeedValidityCheck : public vtkm::worklet::WorkletMapField
  {
    public:
      using ControlSignature = void(FieldIn, FieldOut);
      using ExecutionSignature = void(_1, _2);
    
      VTKM_CONT SeedValidityCheck(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
      {   
        BBox[0] = xmin;
        BBox[1] = xmax;
        BBox[2] = ymin;
        BBox[3] = ymax;
        BBox[4] = zmin;
        BBox[5] = zmax;
      }   
  
      template <typename SeedPortal, typename ValidityPortal>
      VTKM_EXEC void operator()(const SeedPortal &particle, ValidityPortal &validity) const
      {   
        auto p = particle.Pos;
        if(p[0] >= BBox[0] && p[0] <= BBox[1] && p[1] >= BBox[2] && p[1] <= BBox[3] && p[2] >= BBox[4] && p[2] <= BBox[5])
        {   
          validity = 1;
        }   
        else
        {   
          validity = 0;
        }   
      }   

    private:
      double BBox[6];
  };
}

LagrangianField::LagrangianField()
{
  m_minimum = 0.0;
  m_exponent = 1.0;
  m_x_res = 1;
  m_y_res = 1;
  m_z_res = 1;
//  m_boundary_seeds = 0;
}

LagrangianField::~LagrangianField()
{

}

void
LagrangianField::SetField(const std::string &field_name)
{
  m_field_name = field_name;
}

void
LagrangianField::SetDerivedField(const std::string &derived_field)
{
  m_derived_field = derived_field;
}

void
LagrangianField::SetStepSize(const double &step_size)
{
  m_step_size = step_size;
}

void
LagrangianField::SetWriteFrequency(const int &write_frequency)
{
  m_write_frequency = write_frequency;
}

void
LagrangianField::SetExponent(const double &exponent)
{
  m_exponent = exponent;
}

void
LagrangianField::SetMinimum(const double &minimum)
{
  m_minimum = minimum;
}

void
LagrangianField::SetCustomSeedResolution(const int &cust_res)
{
	m_cust_res = cust_res;
}

void
LagrangianField::SetSeedResolutionInX(const int &x_res)
{
	m_x_res = x_res;
}

void
LagrangianField::SetSeedResolutionInY(const int &y_res)
{
	m_y_res = y_res;
}
void
LagrangianField::SetSeedResolutionInZ(const int &z_res)
{
	m_z_res = z_res;
}

void
LagrangianField::SetDomainId(const int &domain_id)
{
  m_domain_id = domain_id;
}

void LagrangianField::PreExecute()
{
  Filter::PreExecute();
  Filter::CheckForRequiredField(m_field_name);
}

void LagrangianField::PostExecute()
{
  Filter::PostExecute();
}

void LagrangianField::DoExecute()
{
  vtkm::Id rank = vtkh::GetMPIRank();
  vtkm::Id dom_id;
  vtkm::cont::DataSet dom;
  int m_strip_ghost_cells = 1;
  if(m_strip_ghost_cells)
  {
    vtkh::GhostStripper stripper;
    stripper.SetInput(this->m_input);
    stripper.SetField("ascent_ghosts");
    stripper.Update();
    vtkh::DataSet *min_dom = stripper.GetOutput();
    min_dom->GetDomain(m_domain_id, dom, dom_id);
  }
  else
  {   
    this->m_input->GetDomain(m_domain_id, dom, dom_id);
  }

  vtkm::Bounds bounds = dom.GetCoordinateSystem().GetBounds();
  double BB[6];
  BB[0] = bounds.X.Min;  
  BB[1] = bounds.X.Max;  
  BB[2] = bounds.Y.Min;  
  BB[3] = bounds.Y.Max;  
  BB[4] = bounds.Z.Min;  
  BB[5] = bounds.Z.Max;   

  vtkm::cont::DynamicCellSet cell_set = dom.GetCellSet();
  vtkm::cont::CellSetStructured<3> cell_set3 = cell_set.Cast<vtkm::cont::CellSetStructured<3>>();
  vtkm::Id3 dims = cell_set3.GetPointDimensions();
      
  int num_gridpts = dims[0]*dims[1]*dims[2];
  int num_cells = (dims[0]-1)*(dims[1]-1)*(dims[2]-1); 
  int num_seeds = (dims[0]/m_x_res)*(dims[1]/m_y_res)*(dims[2]/m_z_res); 

  if(cycle == 0)
  {
    SeedArray.Allocate(num_seeds);
    SeedArrayOriginal.Allocate(num_seeds);
    SeedValidity.Allocate(num_seeds);
  }

  auto seed_validity = SeedValidity.GetPortalControl();
  auto seed_original_portal = SeedArrayOriginal.GetPortalControl();
  auto seed_portal = SeedArray.GetPortalControl();
  
  using vectorField_d = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>>;
  using vectorField_f = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3>>;

  bool field_exists = false;
  vectorField_d velocityArray; 
  velocityArray.Allocate(num_gridpts);
  if(dom.HasField(m_field_name))
  {
    vtkm::cont::VariantArrayHandle field1 = dom.GetField(m_field_name).GetData();
        
    if(!field1.IsType<vectorField_d>() && !field1.IsType<vectorField_f>())
    {   
      throw Error("Vector field type does not match <vtkm::Vec<vtkm::Float32,3>> or <vtkm::Vec<vtkm::Float64,3>>");
    }   
    vtkm::cont::ArrayHandleVirtual<vtkm::Vec<vtkm::Float64,3>> v_FieldArray =  field1.AsVirtual<vtkm::Vec<vtkm::Float64,3>>();
    vtkm::cont::ArrayCopy(v_FieldArray.Cast<vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>>>(), velocityArray);

    field_exists = true;
  }
/*  else if()
  { // In case you want to derive the velocity field from other fields (for example, momentum/density) 
    field_exists = true;
    vtkm::cont::ArrayCopy(derived_velocity, velocityArray);
  } */ 
  else
  {
    throw Error("Domain does not contain specified vector field for Eulerian analysis.");
  }
 
  if(field_exists)
  { 
    if(cycle % m_write_frequency == 0 && cycle != 0)
    { 
      /* Write basis flow information out */

      int connectivity_index = 0;
      std::vector<vtkm::Id> connectivity;
      std::vector<vtkm::Vec<vtkm::Float64, 3>> pointCoordinates;
      std::vector<vtkm::UInt8> shapes;
      std::vector<vtkm::IdComponent> numIndices;
 //     std::vector<vtkm::Id> pathlineId;

      int count_valid = 0;
      
      for(int i = 0; i < num_seeds; i++)
      {
        if(seed_validity.Get(i))
        {
          auto pt1 = seed_original_portal.Get(i).Pos;
          auto pt2 = seed_portal.Get(i).Pos;

          connectivity.push_back(connectivity_index);
          connectivity.push_back(connectivity_index + 1);
          connectivity_index += 2;
          pointCoordinates.push_back(
             vtkm::Vec<vtkm::Float64, 3>(pt1[0], pt1[1], pt1[2]));
          pointCoordinates.push_back(
             vtkm::Vec<vtkm::Float64, 3>(pt2[0], pt2[1], pt2[2]));
          shapes.push_back(vtkm::CELL_SHAPE_LINE);
          numIndices.push_back(2);
//          pathlineId.push_back(i);
          count_valid++;
        }
      }

      vtkm::cont::DataSetBuilderExplicit DSB_Explicit;
      vtkm::cont::DataSet basis = DSB_Explicit.Create(pointCoordinates, shapes, numIndices, connectivity);
//      vtkm::cont::DataSetFieldAdd dsfa; // You can comment these out after debugging is done.
//      dsfa.AddCellField(basis, "ID", pathlineId);

      std::stringstream outputfile;
      outputfile << "/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/" << "Lagrangian_Explicit_" << rank << "_" << cycle << ".vtk";

      vtkm::io::writer::VTKDataSetWriter writer(outputfile.str().c_str());
      writer.WriteDataSet(basis);
      
      std::stringstream boundsfile;
      boundsfile << "/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/" << "Bounds_" << rank << "_" << cycle << ".vtk";
      WriteBoundsInformation(BB, boundsfile.str());
    }
    if(cycle % m_write_frequency == 0)
    {   
      /* Calculate initial set of seed locations using derived fields */

      vtkm::cont::ArrayHandle<vtkm::Float64> featurePointField;
      vtkm::cont::ArrayHandle<vtkm::Float64> featureCellField;
      featurePointField.Allocate(num_gridpts);
      featureCellField.Allocate(num_cells);
      
      using AxisHandle = vtkm::cont::ArrayHandle<vtkm::FloatDefault>;    
      using AxisPortalType = typename AxisHandle::PortalConstControl;                   
                                                                                        
      using RectilinearType = vtkm::cont::ArrayHandleCartesianProduct<AxisHandle, AxisHandle, AxisHandle>;
      using RectilinearPortalType = typename RectilinearType::PortalConstControl;       
                                                                                        
      RectilinearType coords = dom.GetCoordinateSystem().GetData().template Cast<RectilinearType>();
      RectilinearPortalType coords_portal = coords.GetPortalConstControl();    
      AxisPortalType xCoords = coords_portal.GetFirstPortal();     
      AxisPortalType yCoords = coords_portal.GetSecondPortal();    
      AxisPortalType zCoords = coords_portal.GetThirdPortal(); 
      std::vector<vtkm::Float64> xC, yC, zC; 

      for(int i = 0 ; i < xCoords.GetNumberOfValues(); i++)
      {   
        xC.push_back(xCoords.Get(i));
      }   
      for(int i = 0 ; i < yCoords.GetNumberOfValues(); i++)
      {   
        yC.push_back(yCoords.Get(i));
      }   
      for(int i = 0 ; i < zCoords.GetNumberOfValues(); i++)
      {   
        zC.push_back(zCoords.Get(i));
      }   
      
      vtkm::cont::DataSet fieldsDataSet;
      vtkm::cont::DataSetBuilderRectilinear dataSetBuilder;
      fieldsDataSet = dataSetBuilder.Create(xC, yC, zC);
      vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
      dataSetFieldAdd.AddPointField(fieldsDataSet, m_field_name, velocityArray);
      
      if(m_derived_field.compare("entropy") == 0)
      {
        int dim_theta = 20;
        int dim_phi = 10;
        vtkm::cont::ArrayHandle<vtkm::Float64> E_field; 
        E_field.Allocate(num_gridpts);
 
        vtkm::worklet::DispatcherMapField<worklets::VectorEntropy>(worklets::VectorEntropy(dim_theta, dim_phi, dims[0], dims[1], dims[2])).Invoke(E_field, velocityArray);
        
        vtkm::cont::ArrayCopy(E_field, featurePointField);
      
        vtkm::worklet::DispatcherMapField<worklets::PointFieldToCellAverageField>(worklets::PointFieldToCellAverageField(dims[0], dims[1], dims[2])).Invoke(featureCellField, featurePointField);
      
      } 
      else if(m_derived_field.compare("divergence") == 0 || m_derived_field.compare("vorticity") == 0)
      {
        vtkm::cont::DataSet result;
        vtkm::filter::Gradient gradient;
//        gradient.SetComputePointGradient(true); // If set to true - will need modifications to convert point field to average cell field.
        gradient.SetOutputFieldName("Gradient");
        if(m_derived_field.compare("divergence") == 0)
        {
          gradient.SetComputeDivergence(true);
        }
        if(m_derived_field.compare("vorticity") == 0)
        {
          gradient.SetComputeVorticity(true);
        }
        gradient.SetActiveField(m_field_name);
        result = gradient.Execute(fieldsDataSet);
      
        if(m_derived_field.compare("divergence") == 0)
        {
          using dispField_d = vtkm::cont::ArrayHandle<vtkm::Float64>;
          vtkm::cont::VariantArrayHandle field2 = result.GetField("Divergence").GetData();
          vtkm::cont::ArrayHandleVirtual<vtkm::Float64> v_DispArray =  field2.AsVirtual<vtkm::Float64>();
          dispField_d divergenceArray = v_DispArray.Cast<dispField_d>();
          vtkm::cont::ArrayCopy(divergenceArray, featureCellField);
        }
        
        if(m_derived_field.compare("vorticity") == 0)
        {
          vtkm::cont::VariantArrayHandle field3 = result.GetField("Vorticity").GetData();
          vtkm::cont::ArrayHandleVirtual<vtkm::Vec<vtkm::Float64,3>> v_VorticityArray = field3.AsVirtual<vtkm::Vec<vtkm::Float64,3>>();
          vectorField_d vorticityArray = v_VorticityArray.Cast<vectorField_d>();
  
          vtkm::worklet::Magnitude magnitudeWorklet;    
          vtkm::worklet::DispatcherMapField<vtkm::worklet::Magnitude> dispatcher(magnitudeWorklet);
          vtkm::cont::ArrayHandle<vtkm::Float64> vorticity_magnitude;
          vorticity_magnitude.Allocate(vorticityArray.GetNumberOfValues());
          dispatcher.Invoke(vorticityArray, vorticity_magnitude);
          vtkm::cont::ArrayCopy(vorticity_magnitude, featureCellField);
        }
      }
      else
      {
        throw Error("LagrangianField does not currently support the specified derived field.");
      }
        
      vtkm::worklet::DispatcherMapField<worklets::ExponentWorklet>(worklets::ExponentWorklet(m_exponent)).Invoke(featureCellField);
      vtkm::worklet::DispatcherMapField<worklets::AbsoluteValueWorklet>(worklets::AbsoluteValueWorklet()).Invoke(featureCellField);
      vtkm::worklet::DispatcherMapField<worklets::MinimumValueWorklet>(worklets::MinimumValueWorklet(m_minimum)).Invoke(featureCellField);

//      dataSetFieldAdd.AddPointField(fieldsDataSet, m_derived_field, featurePointField);
         
      vtkm::cont::ArrayHandle<vtkm::Float64> scan;
      scan.Allocate(num_cells);
      
      vtkm::cont::Algorithm::ScanInclusive(featureCellField, scan);
      auto scan_portal = scan.GetPortalControl();
      
      int max_val = (int)scan_portal.Get(scan.GetNumberOfValues()-1);
      struct cmwc_state cmwc1, cmwc2;
      unsigned int seed1 = time(NULL);
      initCMWC(&cmwc1, seed1);
      unsigned int seed2 = time(NULL);
      initCMWC(&cmwc2, seed2);

      vtkm::cont::ArrayHandle<vtkm::Float64> randomNumForCells;
      randomNumForCells.Allocate(num_seeds);
      auto rnfc_portal = randomNumForCells.GetPortalControl();

      vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Int32, 3>> randomNumForSeeds;
      randomNumForSeeds.Allocate(num_seeds);
      auto rnfs_portal = randomNumForSeeds.GetPortalControl();

      for(int i = 0; i < num_seeds; i++)
      {
        vtkm::Float64 r = (randCMWC(&cmwc1)%max_val)*1.0d;
        rnfc_portal.Set(i,r);

        vtkm::Int32 a = randCMWC(&cmwc2);
        vtkm::Int32 b = randCMWC(&cmwc2);
        vtkm::Int32 c = randCMWC(&cmwc2);
        rnfs_portal.Set(i, vtkm::Vec<vtkm::Int32, 3>(a, b, c));
      }

      vtkm::cont::ArrayHandle<vtkm::Id> cellIds;
      cellIds.Allocate(num_seeds);

      vtkm::cont::Algorithm::UpperBounds(scan,randomNumForCells,cellIds);
      auto cellids_portal = cellIds.GetPortalControl();

      vtkm::worklet::DispatcherMapField<worklets::RandomSeedInCell>(worklets::RandomSeedInCell(xC, yC, zC, dims[0], dims[1], dims[2])).Invoke(cellIds, randomNumForSeeds, SeedArray);

      vtkm::cont::ArrayCopy(SeedArray, SeedArrayOriginal);
      vtkm::worklet::DispatcherMapField<worklets::SeedValidityCheck>(worklets::SeedValidityCheck(BB[0], BB[1], BB[2], BB[3], BB[4], BB[5])).Invoke(SeedArray, SeedValidity);
   
/*      std::stringstream point_dist;
      point_dist << "/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/Particles_" << rank << "_" << cycle << ".3D";
      std::ofstream out_points;
  
      out_points.open(point_dist.str().c_str(),std::ofstream::out);
      out_points << "X Y Z value\n";
  
      for(int n = 0; n < num_seeds; n++)
      {  
        out_points << seed_portal.Get(n).Pos[0] << " " << seed_portal.Get(n).Pos[1] << " " << 
        seed_portal.Get(n).Pos[2] << " " << seed_validity.Get(n) << "\n"; 
      }   
    
      out_points.close();      
*/

/* Debug section. 
 
      std::stringstream file_stream;
      file_stream << "/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/" << "Field_" << rank << "_" << cycle << ".vtk";
      vtkm::io::writer::VTKDataSetWriter writer(file_stream.str());
      writer.WriteDataSet(fieldsDataSet);

 Debug section ends */
    }   /* End of block to calculate new SeedArray values based on a derived field*/

    const vtkm::cont::DynamicCellSet& cells = dom.GetCellSet();
    const vtkm::cont::CoordinateSystem& coords = dom.GetCoordinateSystem();
  
    using FieldHandle = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>>;
//    using FieldHandle = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>>;
//    FieldHandle velocity_field = dom.GetField(m_field_name).GetData().Cast<FieldHandle>();
    using GridEvalType = vtkm::worklet::particleadvection::GridEvaluator<FieldHandle>;
    using RK4Type = vtkm::worklet::particleadvection::RK4Integrator<GridEvalType>;
    vtkm::worklet::ParticleAdvection particleadvection;
    vtkm::worklet::ParticleAdvectionResult res;
  
    GridEvalType gridEval(coords, cells, velocityArray);
    RK4Type rk4(gridEval, static_cast<vtkm::Float32>(m_step_size));
  
    res = particleadvection.Run(rk4, SeedArray, 1); // Taking a single step
  
//    auto seed_particles = res.Particles;
//    auto seed_particles_portal = seed_particles.GetPortalControl();
  vtkm::cont::ArrayCopy(res.Particles, SeedArray);

    vtkm::worklet::DispatcherMapField<worklets::SeedValidityCheck>(worklets::SeedValidityCheck(BB[0], BB[1], BB[2], BB[3], BB[4], BB[5])).Invoke(SeedArray, SeedValidity);
  } // Field exists and we can perform Lagrangian analysis.

  cycle += 1;
  this->m_output = new DataSet();
  this->m_output->AddDomain(dom, dom_id);
}


//-----------------------------------------------------------------------------
inline void LagrangianField::WriteDataSet(vtkm::Id cycle,
                                     const std::string& filename,
                                     vtkm::cont::DataSet dataset)
{
  std::stringstream file_stream;
  file_stream << filename << cycle << ".vtk";
  vtkm::io::writer::VTKDataSetWriter writer(file_stream.str());
  writer.WriteDataSet(dataset);
}

inline void LagrangianField::WriteBoundsInformation(double *BB, std::string boundsfile)
{
  std::ofstream out_points;
  out_points.open(boundsfile.c_str(),std::ofstream::out);
  out_points << BB[0] << " " << BB[1] << " " << BB[2] << " " << BB[3] << " " << BB[4] << " " << BB[5]; 
  out_points.close(); 
}

std::string
LagrangianField::GetName() const
{
  return "vtkh::LagrangianField";
}

} //  namespace vtkh
