#ifndef VTK_H_LAGRANGIANFIELD_HPP
#define VTK_H_LAGRANGIANFIELD_HPP

#include <vtkh/vtkh_exports.h>
#include <vtkh/vtkh.hpp>
#include <vtkh/filters/Filter.hpp>
#include <vtkh/DataSet.hpp>

namespace vtkh
{

class VTKH_API LagrangianField : public Filter
{
public:
  LagrangianField();
  virtual ~LagrangianField();
  std::string GetName() const override;
	void SetField(const std::string &field_name);
  void SetStepSize(const double &step_size);
  void SetWriteFrequency(const int &write_frequency);
  void SetExponent(const double &exponent);
  void SetMinimum(const double &minimum);
	void SetCustomSeedResolution(const int &cust_res);
	void SetSeedResolutionInX(const int &x_res);
	void SetSeedResolutionInY(const int &y_res);
	void SetSeedResolutionInZ(const int &z_res);
  void SetDerivedField(const std::string &derived_field);
	void SetDomainId(const int &domain_id);

private:
inline void WriteDataSet(vtkm::Id cycle, const std::string& filename, vtkm::cont::DataSet dataset);
inline void WriteBoundsInformation(double *BB, std::string boundsfile); 

protected:
  void PreExecute() override;
  void PostExecute() override;
  void DoExecute() override;

  std::string m_field_name;
  std::string m_derived_field;
	double m_step_size;
	int m_write_frequency;
	double m_exponent, m_minimum;
	int m_cust_res;
	int m_domain_id;
	int m_x_res, m_y_res, m_z_res;
};

} //namespace vtkh
#endif
