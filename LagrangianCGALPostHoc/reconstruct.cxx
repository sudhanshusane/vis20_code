#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstring>
#include <math.h>
#include <string.h>
#include <vector>
#include <omp.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/Types.h>
#include <vtkm/cont/DataSetBuilderExplicit.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/cont/ArrayHandleVirtualCoordinates.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/ParticleAdvection.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/particleadvection/GridEvaluators.h>
#include <vtkm/worklet/particleadvection/Integrators.h>
#include <vtkm/worklet/particleadvection/Particles.h>
#include <vtkm/cont/Algorithm.h>
#include <vtkm/cont/ArrayHandleCast.h>
#include <vtkm/cont/Invoker.h>
#include <map>
#include <iterator>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <list>
#include <mpi.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_3<unsigned int, K> Vb; 
//typedef CGAL::Triangulation_data_structure_3<Vb> Tds;
typedef CGAL::Triangulation_data_structure_3<Vb, CGAL::Triangulation_cell_base_3<K>, CGAL::Parallel_tag> Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds> Triangulation;
typedef Triangulation::Cell_handle Cell_handle;
typedef Triangulation::Vertex_handle Vertex_handle;
typedef Triangulation::Point Vertex;

void crossProduct(double *ans, double *v1, double *v2)
{
  ans[0] = v1[1]*v2[2] - v1[2]*v2[1];
  ans[1] = v1[2]*v2[0] - v1[0]*v2[2];
  ans[2] = v1[0]*v2[1] - v1[1]*v2[0];
  return;
}

double dotProduct( double *v1, double *v2 )
{
  double result = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
  return (result);
}


void bary_tet( double *ans, double *p, double *a, double *b, double *c, double *d )
{
  double vap[3];
  double vbp[3];
  double vcp[3];
  double vdp[3];
  double vab[3];
  double vac[3];
  double vad[3];
  double vbc[3];
  double vbd[3];
  double va;
  double vb;
  double vc;
  double vd;
  double v;
  double temp[3];

  int i;

  for ( i = 0; i < 3; i++ )
  {
    vap[i] = p[i] - a[i];
    vbp[i] = p[i] - b[i];
    vcp[i] = p[i] - c[i];
    vdp[i] = p[i] - d[i];
    vab[i] = b[i] - a[i];
    vac[i] = c[i] - a[i];
    vad[i] = d[i] - a[i];
    vbc[i] = c[i] - b[i];
    vbd[i] = d[i] - b[i];
  }
  crossProduct( temp, vbd, vbc );
  va = dotProduct( vbp, temp ) / 6.0 ;
  crossProduct( temp, vac, vad );
  vb = dotProduct( vap, temp ) / 6.0 ;
  crossProduct( temp, vad, vab );
  vc = dotProduct( vap, temp ) / 6.0 ;
  crossProduct( temp, vab, vac );
  vd = dotProduct( vap, temp ) / 6.0 ;
  crossProduct( temp, vac, vad );
  v  = dotProduct( vab, temp ) / 6.0 ;

  ans[0] = va / v;
  ans[1] = vb / v;
  ans[2] = vc / v;
  ans[3] = vd / v;
  
  return;
}


inline bool AllMessagesReceived(bool *a, int num_ranks)
{
for(int i = 0; i < num_ranks; i++)
{
  if(a[i] == false)
    return false;
}
return true;
}

inline bool BoundsCheck(float x, float y, float z, double *BB)
{
  if(x >= BB[0] && x <= BB[1] && y >= BB[2] && y <= BB[3] && z >= BB[4] && z <= BB[5])
  {
    return true;
  }
  return false;
}

inline bool BoundsCheck(float x, float y, float z, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
{
  if(x >= xmin && x <= xmax && y >= ymin && y <= ymax && z >= zmin && z <= zmax)
  {
    return true;
  }
  return false;
}

std::vector<int> GetNeighborRankList(vtkm::Id num_ranks, vtkm::Id rank, double *bbox_list)
{
    /*  
    * For each node. Calculate 26 query points. Each of these query points is outside the node. 
    * Check if that query point exists in any of the other nodes. 
    * If none, then do nothing. If yes, then add the rank to the vector.
    */

  double xlen = bbox_list[rank*6+1] - bbox_list[rank*6+0];
  double ylen = bbox_list[rank*6+3] - bbox_list[rank*6+2];
  double zlen = bbox_list[rank*6+5] - bbox_list[rank*6+4];

  double mid[3];
  mid[0] = bbox_list[rank*6+0] + (xlen/2.0);
  mid[1] = bbox_list[rank*6+2] + (ylen/2.0);
  mid[2] = bbox_list[rank*6+4] + (zlen/2.0);

  double query_points[26][3] = {{mid[0] + xlen, mid[1], mid[2]},
                                {mid[0] - xlen, mid[1], mid[2]},
                                {mid[0] + xlen, mid[1] + ylen, mid[2]},
                                {mid[0] + xlen, mid[1] - ylen, mid[2]},
                                {mid[0] - xlen, mid[1] + ylen, mid[2]},
                                {mid[0] - xlen, mid[1] - ylen, mid[2]},
                                {mid[0], mid[1] + ylen, mid[2]},
                                {mid[0], mid[1] - ylen, mid[2]},
                                {mid[0] + xlen, mid[1], mid[2] + zlen},
                                {mid[0] - xlen, mid[1], mid[2] + zlen},
                                {mid[0] + xlen, mid[1] + ylen, mid[2] + zlen},
                                {mid[0] + xlen, mid[1] - ylen, mid[2] + zlen},
                                {mid[0] - xlen, mid[1] + ylen, mid[2] + zlen},
                                {mid[0] - xlen, mid[1] - ylen, mid[2] + zlen},
                                {mid[0], mid[1] + ylen, mid[2] + zlen},
                                {mid[0], mid[1] - ylen, mid[2] + zlen},
                                {mid[0], mid[1], mid[2] + zlen},
                                {mid[0] + xlen, mid[1], mid[2] - zlen},
                                {mid[0] - xlen, mid[1], mid[2] - zlen},
                                {mid[0] + xlen, mid[1] + ylen, mid[2] - zlen},
                                {mid[0] + xlen, mid[1] - ylen, mid[2] - zlen},
                                {mid[0] - xlen, mid[1] + ylen, mid[2] - zlen},
                                {mid[0] - xlen, mid[1] - ylen, mid[2] - zlen},
                                {mid[0], mid[1] + ylen, mid[2] - zlen},
                                {mid[0], mid[1] - ylen, mid[2] - zlen},
                                {mid[0], mid[1], mid[2] - zlen}};

  std::vector<int> neighbor_ranks;

  for(int q = 0; q < 26; q++)
  {
    for(int p = 0; p < num_ranks; p++)
    {
      if(p != rank)
      {
        if(BoundsCheck(query_points[q][0], query_points[q][1], query_points[q][2],
        bbox_list[p*6+0], bbox_list[p*6+1], bbox_list[p*6+2], bbox_list[p*6+3], bbox_list[p*6+4], bbox_list[p*6+5]))
        {
          neighbor_ranks.push_back(p);
          break;
        }
      }
    }
  }

  return neighbor_ranks;
}







bool withinBounds(double x, double y, double z, double bbox0, double bbox1, double bbox2, double bbox3, double bbox4, double bbox5)
{
  if(x >= bbox0 && x <= bbox1 && y >= bbox2 && y <= bbox3 && z >= bbox4 && z <= bbox5)
    return true;
  return false;
}

double calculateDistance(double pt1x, double pt1y, double pt1z, double pt2x, double pt2y, double pt2z)
{
  return sqrt(pow(pt2x-pt1x,2) + pow(pt2y-pt1y,2) + pow(pt2z-pt1z,2));
}

int main(int argc, char *argv[])
{
  int ierr, num_ranks, rank;
  std::string m_seed_path = argv[1]; //"/research/Sudhanshu/Cloverleaf3D/Seeds.txt";
  int m_num_seeds = atoi(argv[2]);
  std::string m_basis_path = argv[3]; //"/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/";
  int m_interval = atoi(argv[4]);
  int m_start_cycle = atoi(argv[5]);
  int m_end_cycle = atoi(argv[6]);;
  std::string m_output_path = argv[7]; 
 
  ierr = MPI_Init(&argc, &argv);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  vtkm::cont::ArrayHandle<vtkm::Particle> SeedParticleArray, SeedParticleOriginal;
  SeedParticleArray.Allocate(m_num_seeds);
  SeedParticleOriginal.Allocate(m_num_seeds);

  auto seed_portal = SeedParticleArray.GetPortalControl();
  vtkm::cont::ArrayHandle<vtkm::Id> SeedValidity;
  SeedValidity.Allocate(m_num_seeds);
  auto seed_validity = SeedValidity.GetPortalControl();

  std::ifstream seed_stream(m_seed_path);
  float x1, y1, z1;
  int seed_counter = 0;

  while(seed_stream >> x1)
  {
    seed_stream >> y1;
    seed_stream >> z1;
    seed_portal.Set(seed_counter, vtkm::Particle(vtkm::Vec<vtkm::FloatDefault, 3>(x1,y1,z1),seed_counter));
    seed_counter++;
  }

  vtkm::cont::ArrayCopy(SeedParticleArray, SeedParticleOriginal);
  auto seed_original_portal = SeedParticleOriginal.GetPortalControl();
  
  std::stringstream filename;
  filename << m_basis_path << "Lagrangian_Explicit_" << rank << "_" << m_interval << ".vtk"; // Load the first basis flow file.

//  vtkm::cont::DataSet dataset_info;
//  vtkm::io::reader::VTKDataSetReader reader(filename.str().c_str());
//  dataset_info = reader.ReadDataSet();
 
  std::stringstream boundsfile;
  boundsfile << m_basis_path << "Bounds_" << rank << "_" << m_interval << ".txt";
  std::ifstream bounds_stream(boundsfile.str());
  double BB[6];
  bounds_stream >> BB[0] >> BB[1] >> BB[2] >> BB[3] >> BB[4] >> BB[5];

/** Using extracted BBox information - check seed validity and identify seeds that belong to this block **/
  std::cout << "[" << rank << "] Node BBOX :"
  << BB[0] << " " << BB[1] << " " << BB[2] << " " 
  << BB[3] << " " << BB[4] << " " << BB[5] << std::endl;

#pragma openmp parallel for
  for(int i = 0; i < m_num_seeds; i++)
  {
     auto pt = seed_portal.Get(i).Pos;
     if(BoundsCheck(pt[0], pt[1], pt[2], BB))
     {
       seed_validity.Set(i,1);
     }
     else
     {
       seed_validity.Set(i,0);
     }
  }


/** Sharing bbox information with all other nodes, so that adjacent nodes can be identified. **/

  std::vector<int> neighbor_ranks;
  double *bbox_list = (double*)malloc(sizeof(double)*6*num_ranks);
  bool allReceived[num_ranks] = {false};
  allReceived[rank] = true;

  if(num_ranks > 1)
  {
    bbox_list[rank*6 + 0] = BB[0];
    bbox_list[rank*6 + 1] = BB[1];
    bbox_list[rank*6 + 2] = BB[2];
    bbox_list[rank*6 + 3] = BB[3];
    bbox_list[rank*6 + 4] = BB[4];
    bbox_list[rank*6 + 5] = BB[5];

    for(int i = 0; i < num_ranks; i++)
    { 
      if(i != rank)
      { 
        int ierr = MPI_Bsend(BB, 6, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
      }
    }

    while(!AllMessagesReceived(allReceived, num_ranks))
    { 
      MPI_Status probe_status, recv_status;
      int ierr = MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &probe_status);
      int count;
      MPI_Get_count(&probe_status, MPI_DOUBLE, &count);
      double *recvbuff;
      recvbuff = (double*)malloc(sizeof(double)*count);
      MPI_Recv(recvbuff, count, MPI_DOUBLE, probe_status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &recv_status);
      if(count == 6)
      { 
        bbox_list[6*probe_status.MPI_SOURCE + 0] = recvbuff[0];
        bbox_list[6*probe_status.MPI_SOURCE + 1] = recvbuff[1];
        bbox_list[6*probe_status.MPI_SOURCE + 2] = recvbuff[2];
        bbox_list[6*probe_status.MPI_SOURCE + 3] = recvbuff[3];
        bbox_list[6*probe_status.MPI_SOURCE + 4] = recvbuff[4];
        bbox_list[6*probe_status.MPI_SOURCE + 5] = recvbuff[5];
    
        allReceived[recv_status.MPI_SOURCE] = true;
      }
      else
      {
        std::cout << "[" << rank << "] Corrupt message received from " << probe_status.MPI_SOURCE << std::endl;
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    // Calculate query points and for list of adjacent nodes  
    neighbor_ranks = GetNeighborRankList(num_ranks, rank, bbox_list);
  }

  for(int cycle = m_start_cycle; cycle <= m_end_cycle; cycle += m_interval)
  {
   /* Loop over all ranks (if rank is containined in neighbor_ranks or is == rank -> load data)
    * Read vtk files using VTKM and store data
    * Create triangulation
    * Get cell ids based on valid seed particle locations 
    * Create an explicit dataset and interpolate
    * Create an explicit dataset and probe
    */ 
    std::vector<double> px, py, pz; // Start location of a basis flow
    std::vector<double> dx, dy, dz; // Displacement of corresponding basis flow/ End location of basis flow

    std::stringstream flowmap_name;
    flowmap_name << m_basis_path << "Lagrangian_Explicit_" << rank << "_" << cycle << ".vtk";
    // Node basis flows of the node.
    vtkm::cont::DataSet input_flowmap;
    vtkm::io::reader::VTKDataSetReader reader(flowmap_name.str().c_str());
    input_flowmap = reader.ReadDataSet(); 
    vtkm::cont::ArrayHandleVirtualCoordinates coordinatesystem = input_flowmap.GetCoordinateSystem().GetData();
    int num_basis_points = coordinatesystem.GetNumberOfValues(); 
    auto coords_portal = input_flowmap.GetCoordinateSystem().GetData().GetPortalControl();
    int num_basis = num_basis_points/2;
    for(int i = 0; i < num_basis; i++)
    {
      auto pt0 = coords_portal.Get(i*2+0);
      auto pt1 = coords_portal.Get(i*2+1);
      
      px.push_back(pt0[0]); 
      py.push_back(pt0[1]); 
      pz.push_back(pt0[2]); 

      dx.push_back(pt1[0]); 
      dy.push_back(pt1[1]); 
      dz.push_back(pt1[2]); 
    }
    
    for(int r = 0; r < neighbor_ranks.size(); r++)
    {
      std::stringstream flowmap_name2;
      flowmap_name2 << m_basis_path << "Lagrangian_Explicit_" << neighbor_ranks[r] << "_" << cycle << ".vtk";
      // Node basis flows of the node.
      vtkm::io::reader::VTKDataSetReader reader(flowmap_name2.str().c_str());
      input_flowmap = reader.ReadDataSet(); 
      vtkm::cont::ArrayHandleVirtualCoordinates coordinatesystem2 = input_flowmap.GetCoordinateSystem().GetData();
      int num_basis_points2 = coordinatesystem2.GetNumberOfValues(); 
      auto coords_portal2 = input_flowmap.GetCoordinateSystem().GetData().GetPortalControl();
      int num_basis2 = num_basis_points2/2;
      for(int i = 0; i < num_basis2; i++)
      {
        auto pt0 = coords_portal2.Get(i*2+0);
        auto pt1 = coords_portal2.Get(i*2+1);
        
        px.push_back(pt0[0]); 
        py.push_back(pt0[1]); 
        pz.push_back(pt0[2]); 

        dx.push_back(pt1[0]); 
        dy.push_back(pt1[1]); 
        dz.push_back(pt1[2]); 
      }
    }
    
    Triangulation::Lock_data_structure locking_ds(CGAL::Bbox_3(),50);
//      CGAL::Bbox_3(maxBbox[0], maxBbox[2], maxBbox[4], maxBbox[1], maxBbox[3], maxBbox[5]),50);
    Triangulation current_cgal_mesh;
    std::vector<std::pair<Vertex,int>> current_basis;
    for(int b = 0; b < px.size(); b++)
    {   
      current_basis.push_back(std::make_pair(Vertex(px[b], py[b], pz[b]),b));  // Attach ref to find end points for probe.
    }  
    current_cgal_mesh.insert(current_basis.begin(), current_basis.end(), &locking_ds);

    for(int i = 0; i < m_num_seeds; i++)
    {
      if(seed_validity.Get(i))
      {
//        current_cgal_mesh.Locate();
// Get a cell handle. Else Make the seed_validity = 0
// Once you have all the vertices
// Perform a vtkm probe or write your own tetrahedron interpolation! 
        auto s = seed_portal.Get(i).Pos;
        Vertex_handle v;
        Cell_handle cell;
        cell = current_cgal_mesh.locate(Vertex(s[0], s[1], s[2]), cell);
        
        if(current_cgal_mesh.is_infinite(cell))
        {   
          std::cout << "Infinite cell : " << i << std::endl;
          seed_validity.Set(i, 0);
          continue;
        }  
 
        int index1 = cell->vertex(0)->info();
        int index2 = cell->vertex(1)->info();
        int index3 = cell->vertex(2)->info();
        int index4 = cell->vertex(3)->info();
        
        double a[3], b[3], c[3], d[3];
        double BC[4]; 
        double p[3];
        
        a[0] = px[index1];
        a[1] = py[index1];
        a[2] = pz[index1];
        
        b[0] = px[index2];
        b[1] = py[index2];
        b[2] = pz[index2];

        c[0] = px[index3];
        c[1] = py[index3];
        c[2] = pz[index3];

        d[0] = px[index4];
        d[1] = py[index4];
        d[2] = pz[index4];

        p[0] = s[0];
        p[1] = s[1];
        p[2] = s[2];

        bary_tet(BC, p, a, b, c, d); 
        
        double interp[3];
        interp[0] = BC[0]*dx[index1] + BC[1]*dx[index2] + BC[2]*dx[index3] + BC[3]*dx[index4];
        interp[1] = BC[0]*dy[index1] + BC[1]*dy[index2] + BC[2]*dy[index3] + BC[3]*dy[index4];
        interp[2] = BC[0]*dz[index1] + BC[1]*dz[index2] + BC[2]*dz[index3] + BC[3]*dz[index4];
        
        seed_portal.Set(i, vtkm::Particle(vtkm::Vec<vtkm::FloatDefault, 3>(interp[0], interp[1], interp[2]), i));
      }
    }

      /* Write Output Start */

    int connectivity_index = 0;
    std::vector<vtkm::Id> connectivity;
    std::vector<vtkm::Vec<vtkm::Float64, 3>> pointCoordinates;
    std::vector<vtkm::UInt8> shapes;
    std::vector<vtkm::IdComponent> numIndices;
    std::vector<vtkm::Id> pathlineId;

    for(int i = 0; i < m_num_seeds; i++)
    {   
      if(seed_validity.Get(i))
      {   
        auto pt1 = seed_original_portal.Get(i).Pos;
        auto pt2 = seed_portal.Get(i).Pos;

        connectivity.push_back(connectivity_index);
        connectivity.push_back(connectivity_index + 1); 
        connectivity_index += 2;
        pointCoordinates.push_back(
           vtkm::Vec<vtkm::Float64, 3>(pt1[0], pt1[1], pt1[2]));
        pointCoordinates.push_back(
           vtkm::Vec<vtkm::Float64, 3>(pt2[0], pt2[1], pt2[2]));
        shapes.push_back(vtkm::CELL_SHAPE_LINE);
        numIndices.push_back(2);
        pathlineId.push_back(i);
      }   
    }   

    vtkm::cont::DataSetBuilderExplicit DSB_Explicit;
    vtkm::cont::DataSet pathlines = DSB_Explicit.Create(pointCoordinates, shapes, numIndices, connectivity);
    vtkm::cont::DataSetFieldAdd dsfa;
    dsfa.AddCellField(pathlines, "ID", pathlineId);

    std::stringstream outputfile;
    outputfile << m_output_path << rank << "_" << cycle << ".vtk";

    vtkm::io::writer::VTKDataSetWriter writer(outputfile.str().c_str());
    writer.WriteDataSet(pathlines);
    /* Write Output End */  
 
    /* Exchange information - Update Seed Information Start */

    std::vector<int> outgoing_id;
    std::vector<int> outgoing_dest;

    for(int i = 0; i < m_num_seeds; i++)
    {
      if(seed_validity.Get(i))
      {
        auto pt = seed_portal.Get(i).Pos;
        if(!BoundsCheck(pt[0], pt[1], pt[2], BB))
        {
          seed_validity.Set(i, 0);
          int dest = -1;
          for(int r = 0; r < num_ranks; r++)
          {
            if(r != rank)
            {
              if(BoundsCheck(pt[0], pt[1], pt[2],
           bbox_list[r*6 + 0], bbox_list[r*6 + 1], bbox_list[r*6 + 2], bbox_list[r*6 + 3], bbox_list[r*6 + 4], bbox_list[r*6 + 5]))
              {
                dest = r;
                break;
              }
            }
          }
          if(dest != -1)
          {
            outgoing_id.push_back(i);
            outgoing_dest.push_back(dest);
          }
        }
      }
    }

    if(num_ranks > 1)
    {
      MPI_Barrier(MPI_COMM_WORLD);

      int bufsize = 10*(m_num_seeds*5 + (MPI_BSEND_OVERHEAD * num_ranks)); // message packet size 4 + message packet size 1 for empty sends.
      double *buf = (double*)malloc(sizeof(double)*bufsize);
      MPI_Buffer_attach(buf, bufsize);
      // Sending messages to all ranks.
      // Message format: ID X Y Z ID X Y Z ..
      // If not particles to send: -1 
      for(int r = 0; r < num_ranks; r++)
      {
        if(r != rank)
        {
          int particles_to_send = 0;
          std::vector<double> message;
          for(int j = 0; j < outgoing_dest.size(); j++)
          {
            if(outgoing_dest[j] == r)
            {
              auto pt = seed_portal.Get(outgoing_id[j]).Pos;
              particles_to_send++;
              message.push_back(outgoing_id[j]);
              message.push_back(pt[0]);
              message.push_back(pt[1]);
              message.push_back(pt[2]);
            }
          }

          int buffsize = particles_to_send*4;
          double *sendbuff;


          if(buffsize == 0)
            sendbuff = (double*)malloc(sizeof(double));
          else
            sendbuff = (double*)malloc(sizeof(double)*buffsize);

          if(buffsize  == 0)
          {
            buffsize = 1;
            sendbuff[0] = -1.0;
          }
          else
          {
            for(int k = 0; k < message.size(); k++)
            {
              sendbuff[k] = message[k];
            }
          }
          int ierr = MPI_Bsend(sendbuff, buffsize, MPI_DOUBLE, r, 13, MPI_COMM_WORLD);
          free(sendbuff);
        }
      }   // Loop over all ranks.
      MPI_Barrier(MPI_COMM_WORLD);

      // Receive Messages
      bool allReceived2[num_ranks] = {false};
      allReceived2[rank] = true;

      while(!AllMessagesReceived(allReceived2, num_ranks))
      {
        MPI_Status probe_status, recv_status;
        int ierr = MPI_Probe(MPI_ANY_SOURCE, 13, MPI_COMM_WORLD, &probe_status);
        int count;
        MPI_Get_count(&probe_status, MPI_DOUBLE, &count);
        double *recvbuff;
        recvbuff = (double*)malloc(sizeof(double)*count);
        MPI_Recv(recvbuff, count, MPI_DOUBLE, probe_status.MPI_SOURCE, 13, MPI_COMM_WORLD, &recv_status);
        if(count == 1)
        {
          allReceived2[recv_status.MPI_SOURCE] = true;
        }
        else if(count % 4 == 0)
        {
          int num_particles = count/4;

          for(int i = 0; i < num_particles; i++)
          {
            int index = recvbuff[i*4+0];
            seed_validity.Set(index, 1);
            seed_portal.Set(index, vtkm::Particle(vtkm::Vec<vtkm::FloatDefault, 3>
            (recvbuff[i*4+1], recvbuff[i*4+2], recvbuff[i*4+3]), index));
          }
          allReceived2[recv_status.MPI_SOURCE] = true;
        }
        else
        {
          std::cout << "[" << rank << "] Received message of invalid length from : " << recv_status.MPI_SOURCE << std::endl;
          allReceived2[recv_status.MPI_SOURCE] = true;
        }
        free(recvbuff);
      }
      MPI_Buffer_detach(&buf, &bufsize);
      free(buf);
      MPI_Barrier(MPI_COMM_WORLD);
    } // endif rank > 1

    vtkm::cont::ArrayCopy(SeedParticleArray, SeedParticleOriginal);

  }

  MPI_Finalize();
/*
* Find all the bboxs for BTO ranks.
* For each time interval. 
* For each mpi rank.
* For all ranks that are itself/neighbors. Load and triangulate BTO.
* For each seed in mpi rank -- perform vtkprobe. 
* Measure distance and write to rank specific vtk distance field.
* Aggregate distances and divide by total number of seeds to get an average.  
*
* Additional things to output to a file. 
* Number of basis flows exited per rank per interval. Aggregate for each cycle. 
*/

/*  int start = atoi(argv[1]);
  int end = atoi(argv[2]);
  int interval = atoi(argv[3]);
  int num_nodes = atoi(argv[4]);
  std::string inputfolder_bto = argv[5];
  std::string inputfolder_mpi = argv[6];
  double maxBbox[6];
  maxBbox[0] = atof(argv[7]);
  maxBbox[1] = atof(argv[8]);
  maxBbox[2] = atof(argv[9]);
  maxBbox[3] = atof(argv[10]);
  maxBbox[4] = atof(argv[11]);
  maxBbox[5] = atof(argv[12]);
  
  double node_bounds[num_nodes*6];
  std::vector<std::vector<int>> node_neighbors; 
  
  vtkSmartPointer<vtkDataSetReader> node_reader = vtkSmartPointer<vtkDataSetReader>::New();
  vtkSmartPointer<vtkDataSet> input; 
  // Loop to get bounding box information for each domain.
  for(int i =0; i < num_nodes; i++)
  {
    std::stringstream file_name;
    file_name << inputfolder_bto << "basisflows_" << i << "_" << start << ".vtk";
  
    node_reader->SetFileName(file_name.str().c_str());
    node_reader->Update();
    input = node_reader->GetOutput();
    double bbox[6];
    input->GetBounds(bbox);
    node_bounds[i*6 + 0] = bbox[0];
    node_bounds[i*6 + 1] = bbox[1];
    node_bounds[i*6 + 2] = bbox[2];
    node_bounds[i*6 + 3] = bbox[3];
    node_bounds[i*6 + 4] = bbox[4];
    node_bounds[i*6 + 5] = bbox[5];
  }

  // Loop to identify neighbors.
  for(int i = 0; i < num_nodes; i++)
  {   
    double xlen = node_bounds[i*6 + 1] - node_bounds[i*6 + 0]; 
    double ylen = node_bounds[i*6 + 3] - node_bounds[i*6 + 2]; 
    double zlen = node_bounds[i*6 + 5] - node_bounds[i*6 + 4]; 

    double mid[3];
    mid[0] = node_bounds[i*6 + 0] + (xlen/2.0);
    mid[1] = node_bounds[i*6 + 2] + (ylen/2.0);
    mid[2] = node_bounds[i*6 + 4] + (zlen/2.0);

    double query_points[26][3] = {{mid[0] + xlen, mid[1], mid[2]}, 
                                  {mid[0] - xlen, mid[1], mid[2]},
                                  {mid[0] + xlen, mid[1] + ylen, mid[2]}, 
                                  {mid[0] + xlen, mid[1] - ylen, mid[2]},
                                  {mid[0] - xlen, mid[1] + ylen, mid[2]}, 
                                  {mid[0] - xlen, mid[1] - ylen, mid[2]},
                                  {mid[0], mid[1] + ylen, mid[2]}, 
                                  {mid[0], mid[1] - ylen, mid[2]},
                                  {mid[0] + xlen, mid[1], mid[2] + zlen}, 
                                  {mid[0] - xlen, mid[1], mid[2] + zlen},
                                  {mid[0] + xlen, mid[1] + ylen, mid[2] + zlen}, 
                                  {mid[0] + xlen, mid[1] - ylen, mid[2] + zlen},
                                  {mid[0] - xlen, mid[1] + ylen, mid[2] + zlen},
                                  {mid[0] - xlen, mid[1] - ylen, mid[2] + zlen},
                                  {mid[0], mid[1] + ylen, mid[2] + zlen},
                                  {mid[0], mid[1] - ylen, mid[2] + zlen},
                                  {mid[0], mid[1], mid[2] + zlen},
                                  {mid[0] + xlen, mid[1], mid[2] - zlen},
                                  {mid[0] - xlen, mid[1], mid[2] - zlen},
                                  {mid[0] + xlen, mid[1] + ylen, mid[2] - zlen},
                                  {mid[0] + xlen, mid[1] - ylen, mid[2] - zlen},
                                  {mid[0] - xlen, mid[1] + ylen, mid[2] - zlen},
                                  {mid[0] - xlen, mid[1] - ylen, mid[2] - zlen},
                                  {mid[0], mid[1] + ylen, mid[2] - zlen},
                                  {mid[0], mid[1] - ylen, mid[2] - zlen},
                                  {mid[0], mid[1], mid[2] - zlen}};

    std::vector<int> neighbors;
    for(int q = 0; q < 26; q++)
    {   
      for(int p = 0; p < num_nodes; p++)
      {   
        if(p != i)
        {  
          if(withinBounds(query_points[q][0], query_points[q][1], query_points[q][2], node_bounds[p*6+0], node_bounds[p*6+1]
          , node_bounds[p*6+2], node_bounds[p*6+3], node_bounds[p*6+4], node_bounds[p*6+5]))
          {   
            neighbors.push_back(p);
            break;
          }   
        }   
      }   
    } 
    node_neighbors.push_back(neighbors);  
    // Each nodes neighbors are stored in a list. 
  }

  // Debug.

//  for(int i = 0; i < num_nodes; i++)
//  {
//    for(int j = 0; j < node_neighbors[i].size(); j++)
//    {
//      std::cout << "Node: " << i << " has neighbor : " << node_neighbors[i][j] << std::endl;
//    }
  }
  

  std::cout << "Interval,Terminated,AvgNodeError,MaxNodeError,MinNodeError" << std::endl; 
  int totalTerminated; 
  double maxNodeError, minNodeError;
  for(int c = start; c <= end; c += interval)
  {
    std::cerr << "On interval: " << c << std::endl;
    std::vector<double> nodeError;
    totalTerminated = 0;
    maxNodeError = 0.0;
    minNodeError = 1000.0;

    for(int n = 0; n < num_nodes; n++)
    {
      std::cerr << "On node: " << n << "," << c << std::endl;
      // Read MPI rank file. 
      // Store starting and end locations of basis flows.
      // Starting locations are seed locations for vtkProbe.
      // Load all the neighbors of rank for BTO and triangulate.
      // For each seed, query triangulation and perform vtkprobe. This can be done in parallel. 
      // Store information into an output array - distance field.   
      // Aggregate information - see how many the BTO rank lost.
      // Outside parallel section - write output vtk file. 
      std::stringstream mpi_file;
      mpi_file<< inputfolder_mpi << "basisflows_" << n << "_" << c << ".vtk";
      node_reader->SetFileName(mpi_file.str().c_str());
      node_reader->Update();
      input = node_reader->GetOutput();
      int numseeds = input->GetNumberOfPoints()/2;
      std::vector<double> sx0,sy0,sz0,sx1,sy1,sz1; // This is the groundtruth 
      std::vector<double> bx0,by0,bz0,bx1,by1,bz1;
      
      for(int i = 0; i < numseeds; i++)
      {
        double pt0[3], pt1[3];
        input->GetPoint(i*2+0,pt0);
        input->GetPoint(i*2+1,pt1);
        sx0.push_back(pt0[0]);
        sy0.push_back(pt0[1]);
        sz0.push_back(pt0[2]);
        sx1.push_back(pt1[0]);
        sy1.push_back(pt1[1]);
        sz1.push_back(pt1[2]);
      }
     
      for(int r = 0; r < num_nodes; r++)
      {
        if(std::count(node_neighbors[n].begin(), node_neighbors[n].end(), r) || r == n)
        {
          std::stringstream bto_file;
          bto_file << inputfolder_bto << "basisflows_" << r << "_" << c << ".vtk";
          node_reader->SetFileName(bto_file.str().c_str());
          node_reader->Update();
          input = node_reader->GetOutput();
          int numbasis = input->GetNumberOfPoints()/2;
    
          for(int i = 0; i < numbasis; i++)
          {
            double pt0[3], pt1[3];
            input->GetPoint(i*2+0,pt0);
            input->GetPoint(i*2+1,pt1);
            bx0.push_back(pt0[0]);
            by0.push_back(pt0[1]);
            bz0.push_back(pt0[2]);
            bx1.push_back(pt1[0]);
            by1.push_back(pt1[1]);
            bz1.push_back(pt1[2]);
          }
          if(r == n)
          {
//            std::cout << "Rank [" << r << "] terminated : " << numseeds - numbasis << " at cycle - " << c << std::endl;
              totalTerminated += (numseeds - numbasis);
          }
        }
      }
    
      // Triangulate the basis flows.
      // Compute each seeds next location in parallel. 
      std::vector<double> rx1(numseeds),ry1(numseeds),rz1(numseeds), dist_field(numseeds);
  
      Triangulation::Lock_data_structure locking_ds(CGAL::Bbox_3(),50);
//      CGAL::Bbox_3(maxBbox[0], maxBbox[2], maxBbox[4], maxBbox[1], maxBbox[3], maxBbox[5]),50);
      Triangulation current_cgal_mesh;
      std::vector<std::pair<Vertex,int>> current_basis;
      for(int b = 0; b < bx0.size(); b++)
      {   
        current_basis.push_back(std::make_pair(Vertex(bx0[b], by0[b], bz0[b]),b));  // Attach ref to find end points for probe.
      }  
      current_cgal_mesh.insert(current_basis.begin(), current_basis.end(), &locking_ds);
      //cout << "Mesh is valid: " << current_cgal_mesh.is_valid() <<  " vertices: " << current_cgal_mesh.number_of_vertices() << endl;         

      vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
      vtkSmartPointer<vtkDoubleArray> xvec = vtkSmartPointer<vtkDoubleArray>::New();
      vtkSmartPointer<vtkDoubleArray> yvec = vtkSmartPointer<vtkDoubleArray>::New();
      vtkSmartPointer<vtkDoubleArray> zvec = vtkSmartPointer<vtkDoubleArray>::New();

      xvec->SetNumberOfValues(bx0.size());
      yvec->SetNumberOfValues(bx0.size());
      zvec->SetNumberOfValues(bx0.size());

      xvec->SetName("xvec");
      yvec->SetName("yvec");
      zvec->SetName("zvec");

      for(int b = 0; b < bx0.size(); b++)
      {
        points->InsertNextPoint(bx0[b], by0[b], bz0[b]);
        xvec->SetValue(b, bx1[b]);
        yvec->SetValue(b, by1[b]);
        zvec->SetValue(b, bz1[b]);
      }


      vtkSmartPointer<vtkUnstructuredGrid> seedMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
      seedMesh->SetPoints(points);
      seedMesh->GetPointData()->AddArray(xvec);
      seedMesh->GetPointData()->AddArray(yvec);
      seedMesh->GetPointData()->AddArray(zvec);

      vtkSmartPointer<vtkDelaunay3D> triangulation = vtkSmartPointer<vtkDelaunay3D>::New();
      triangulation->SetInputData(seedMesh);
      triangulation->Update();

      vtkSmartPointer<vtkUnstructuredGrid> triangulated_seedMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
      triangulated_seedMesh = triangulation->GetOutput();

      vtkSmartPointer<vtkPoints> particlePoint = vtkSmartPointer<vtkPoints>::New();

      for(int p = 0; p < numseeds; p++)
      {
        particlePoint->InsertNextPoint(sx0[p], sy0[p], sz0[p]);
      }

      vtkSmartPointer<vtkUnstructuredGrid> particleMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
      particleMesh->SetPoints(particlePoint);

      vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();

      probe->SetSourceData(triangulated_seedMesh);
      probe->SetInputData(particleMesh);
      probe->Update();

      vtkSmartPointer<vtkIntArray> validPts = vtkSmartPointer<vtkIntArray>::New();
      validPts->DeepCopy(probe->GetOutput()->GetPointData()->GetArray(probe->GetValidPointMaskArrayName()));

      vtkSmartPointer<vtkDoubleArray> xlocation =
      vtkDoubleArray::SafeDownCast(probe->GetOutput()->GetPointData()->GetArray("xvec"));
      vtkSmartPointer<vtkDoubleArray> ylocation =
      vtkDoubleArray::SafeDownCast(probe->GetOutput()->GetPointData()->GetArray("yvec"));
      vtkSmartPointer<vtkDoubleArray> zlocation =
      vtkDoubleArray::SafeDownCast(probe->GetOutput()->GetPointData()->GetArray("zvec"));

      for(int p = 0; p < numseeds; p++)
      {
        if(validPts->GetValue(p) == 1)
        {
          rx1[p] = xlocation->GetValue(p);
          ry1[p] = ylocation->GetValue(p);
          rz1[p] = zlocation->GetValue(p);
        }
      }

      std::cerr << "Starting interpolation for node " << n << std::endl;
      #pragma omp parallel for
      for(int p = 0; p < numseeds; p++)
      {
        Vertex_handle v;
        Cell_handle cell;
        cell = current_cgal_mesh.locate(Vertex(sx0[p], sy0[p], sz0[p]), cell);
        
        if(current_cgal_mesh.is_infinite(cell))
        {   
//          std::cout << "Infinite cell : " << p << std::endl;
          rx1[p] = sx1[p];
          ry1[p] = sy1[p];
          rz1[p] = sz1[p];            
          continue;
        }   
        Vertex a(cell->vertex(0)->point());
        Vertex b(cell->vertex(1)->point());
        Vertex c(cell->vertex(2)->point());
        Vertex d(cell->vertex(3)->point());

        int index1 = cell->vertex(0)->info();
        int index2 = cell->vertex(1)->info();
        int index3 = cell->vertex(2)->info();
        int index4 = cell->vertex(3)->info();

        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        vtkSmartPointer<vtkDoubleArray> xvec = vtkSmartPointer<vtkDoubleArray>::New();
        vtkSmartPointer<vtkDoubleArray> yvec = vtkSmartPointer<vtkDoubleArray>::New();
        vtkSmartPointer<vtkDoubleArray> zvec = vtkSmartPointer<vtkDoubleArray>::New();

        xvec->SetNumberOfValues(4);
        yvec->SetNumberOfValues(4);
        zvec->SetNumberOfValues(4);

        xvec->SetName("xvec");
        yvec->SetName("yvec");
        zvec->SetName("zvec");

        points->InsertNextPoint(bx0[index1], by0[index1], bz0[index1]); 
        xvec->SetValue(0, bx1[index1]);
        yvec->SetValue(0, by1[index1]);
        zvec->SetValue(0, bz1[index1]);
        
        points->InsertNextPoint(bx0[index2], by0[index2], bz0[index2]); 
        xvec->SetValue(1, bx1[index2]);
        yvec->SetValue(1, by1[index2]);
        zvec->SetValue(1, bz1[index2]);

        points->InsertNextPoint(bx0[index3], by0[index3], bz0[index3]); 
        xvec->SetValue(2, bx1[index3]);
        yvec->SetValue(2, by1[index3]);
        zvec->SetValue(2, bz1[index3]);
  
        points->InsertNextPoint(bx0[index4], by0[index4], bz0[index4]); 
        xvec->SetValue(3, bx1[index4]);
        yvec->SetValue(3, by1[index4]);
        zvec->SetValue(3, bz1[index4]);
        
        vtkSmartPointer<vtkUnstructuredGrid> seedMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
        seedMesh->SetPoints(points);
        seedMesh->GetPointData()->AddArray(xvec);
        seedMesh->GetPointData()->AddArray(yvec);
        seedMesh->GetPointData()->AddArray(zvec);

        vtkSmartPointer<vtkDelaunay3D> triangulation = vtkSmartPointer<vtkDelaunay3D>::New();
        triangulation->SetInputData(seedMesh);
        triangulation->Update();

        vtkSmartPointer<vtkUnstructuredGrid> triangulated_seedMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
        triangulated_seedMesh = triangulation->GetOutput();

        vtkSmartPointer<vtkPoints> particlePoint = vtkSmartPointer<vtkPoints>::New();

        particlePoint->InsertNextPoint(sx0[p], sy0[p], sz0[p]);

        vtkSmartPointer<vtkUnstructuredGrid> particleMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
        particleMesh->SetPoints(particlePoint);

        vtkSmartPointer<vtkProbeFilter> probe = vtkSmartPointer<vtkProbeFilter>::New();

        probe->SetSourceData(triangulated_seedMesh);
        probe->SetInputData(particleMesh);
        probe->Update();

        vtkSmartPointer<vtkIntArray> validPts = vtkSmartPointer<vtkIntArray>::New();
        validPts->DeepCopy(probe->GetOutput()->GetPointData()->GetArray(probe->GetValidPointMaskArrayName()));

        vtkSmartPointer<vtkDoubleArray> xlocation =
        vtkDoubleArray::SafeDownCast(probe->GetOutput()->GetPointData()->GetArray("xvec"));
        vtkSmartPointer<vtkDoubleArray> ylocation =
        vtkDoubleArray::SafeDownCast(probe->GetOutput()->GetPointData()->GetArray("yvec"));
        vtkSmartPointer<vtkDoubleArray> zlocation =
        vtkDoubleArray::SafeDownCast(probe->GetOutput()->GetPointData()->GetArray("zvec"));
        if(validPts->GetValue(0) == 1)
        {
          rx1[p] = xlocation->GetValue(0);
          ry1[p] = ylocation->GetValue(0);
          rz1[p] = zlocation->GetValue(0);
        }
        else
        {
          rx1[p] = sx1[p];
          ry1[p] = sy1[p];
          rz1[p] = sz1[p];            
        }
        double dist = calculateDistance(sx1[p],sy1[p],sz1[p],rx1[p],ry1[p],rz1[p]);
        dist_field[p] = dist;
      }
      double sum_dist = 0.0;
      
    //  vtkSmartPointer<vtkPoints> outpts = vtkSmartPointer<vtkPoints>::New();
    //  vtkSmartPointer<vtkDoubleArray> dfield = vtkSmartPointer<vtkDoubleArray>::New();
    //  dfield->SetNumberOfValues(numseeds);
    //  dfield->SetName("distance");
      double maxdist = 0.0;
      for(int p = 0; p < numseeds; p++)
      {
        sum_dist += dist_field[p];
     //   outpts->InsertNextPoint(sx1[p],sy1[p],sz1[p]);
     //   dfield->SetValue(p,dist_field[p]);
     //   if(dist_field[p] > maxdist)
     //     maxdist = dist_field[p];
      }
//      std::cout << "[" << n << "] Average distance: " << sum_dist/numseeds << " cycle:" << c << std::endl;
//      std::cout << "[" << n << "] Maximum Error: " << maxdist << " cycle:" << c << std::endl;

    // vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    // unstructuredGrid->SetPoints(outpts);
    //  unstructuredGrid->GetPointData()->AddArray(dfield);
      
    // Write file
    //  std::stringstream outfile;
    //  outfile << inputfolder_bto << "distfield_" << n << "_" << c << ".vtk";
    //  vtkSmartPointer<vtkUnstructuredGridWriter> writer =
    //  vtkSmartPointer<vtkUnstructuredGridWriter>::New();
    //  writer->SetFileName(outfile.str().c_str());
    //  writer->SetInputData(unstructuredGrid);
    //  writer->Write();
    
      double l2error = sum_dist/numseeds;
      nodeError.push_back(l2error);
      if(maxNodeError < l2error)
      {
        maxNodeError = l2error;
      }
      if(minNodeError > l2error)
      {
        minNodeError = l2error;
      }
    }
    double avgNodeErrorSum = 0.0;
    double avgNodeError = 0.0;
    for(int i = 0; i < num_nodes; i++)
    {
      avgNodeErrorSum += nodeError[i];
    }
    avgNodeError = avgNodeErrorSum/num_nodes;
    std::cerr << "Calculated average node error" << std::endl;
    std::cout << c << "," << totalTerminated << "," << avgNodeError << "," << maxNodeError << "," << minNodeError << "\n";

  } 
*/
}

