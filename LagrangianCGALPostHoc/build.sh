#!/bin/bash

echo "Cleaning directory and compiling"

rm -rf CMakeCache.txt CMakeFiles/
rm Reconstruct
cmake -DCGAL_DIR=/home/users/ssane/cgal \
      -DCMAKE_C_COMPILER=/usr/bin/mpicc \
      -DCMAKE_CXX_COMPILER=/usr/bin/mpicxx \
      -DVTK_DIR=/home/users/ssane/VTK-6.3.0 \
      -DBOOST_ROOT=/home/users/ssane/Repositories/Boost/boost_1_61_0 \
      -DBOOST_LIBRARYDIR=/home/users/ssane/Repositories/Boost/boost_1_61_0/install/lib \
      -DMPI_C_INCLUDE_PATH=/usr/lib/x86_64-linux-gnu/openmpi/include \
      -DMPI_C_LIBRARIES=/usr/lib/x86_64-linux-gnu/openmpi/lib/libmpi.so \
      -DMPI_CXX_LIBRARIES=/usr/lib/x86_64-linux-gnu/openmpi/lib/libmpi.so \
      -DCMAKE_BUILD_TYPE="Release" \
      -DCGAL_HEADER_ONLY="ON"
      .
make
echo "Completed."
