#!/bin/bash

START=20

END=180
INTERVAL=$START
TEST="T"
NUM_MPI=64
SEED_PATH="/research/Sudhanshu/Cloverleaf3D/Seeds.txt"
NUM_SEEDS=1000
INPUT_FOLDER="/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/"
OUTPUT_FOLDER="/research/Sudhanshu/Cloverleaf3D/LagrangianField/Vis/Pathlines/Pathlines_"

mpirun -n 8 ./CGAL_Reconstruct $SEED_PATH $NUM_SEEDS $INPUT_FOLDER $INTERVAL $START $END $OUTPUT_FOLDER $NUM_MPI
