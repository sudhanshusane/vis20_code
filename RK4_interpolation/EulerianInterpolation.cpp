#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include <vtkh/vtkh.hpp>
#include <vtkh/Error.hpp>
#include <vtkh/filters/EulerianInterpolation.hpp>
#include <vtkh/DataSet.hpp>
#include <vtkm/Types.h>
#include <vtkm/cont/DataSetBuilderExplicit.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/ParticleAdvection.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/particleadvection/GridEvaluators.h>
#include <vtkm/worklet/particleadvection/Integrators.h>
#include <vtkm/worklet/particleadvection/Particles.h>

#ifdef VTKH_PARALLEL
#include <mpi.h>
#endif

#define MSG_LENGTH 7

namespace vtkh
{

EulerianInterpolation::EulerianInterpolation()
{
}

EulerianInterpolation::~EulerianInterpolation()
{
}

void
EulerianInterpolation::SetField(const std::string &field_name)
{
  m_field_name = field_name;
}

void EulerianInterpolation::SetSeedPath(const std::string &seed_path)
{
  m_seed_path = seed_path;
}

void EulerianInterpolation::SetOutputPath(const std::string &output_path)
{
  m_output_path = output_path;
}

void EulerianInterpolation::SetInputPath(const std::string &input_path)
{
  m_input_path = input_path;
}

void EulerianInterpolation::SetStepSize(const double &step_size)
{
  m_step_size = step_size;
}

void EulerianInterpolation::SetNumSeeds(const int &num_seeds)
{
  m_num_seeds = num_seeds;
}

void EulerianInterpolation::SetInterval(const int &interval)
{
  m_interval = interval;
}

void EulerianInterpolation::SetStartCycle(const int &start_cycle)
{
  m_start_cycle = start_cycle;
}

void EulerianInterpolation::SetEndCycle(const int &end_cycle)
{
  m_end_cycle = end_cycle;
}

void EulerianInterpolation::PreExecute()
{
  Filter::PreExecute();
  Filter::CheckForRequiredField(m_field_name);
}

void EulerianInterpolation::PostExecute()
{
  Filter::PostExecute();
}

void EulerianInterpolation::DoExecute()
{
#ifdef VTKH_PARALLEL

  vtkm::Id rank = vtkh::GetMPIRank();
  vtkm::Id num_ranks = vtkh::GetMPISize();

/** PREPROCESSING **/
/** 1. READING SEED FILE FROM DISK **/

  vtkm::cont::ArrayHandle<vtkm::Particle> SeedParticleArray, SeedParticleOriginal;
  vtkm::cont::ArrayHandle<vtkm::Id> SeedValidity;
  SeedParticleArray.Allocate(m_num_seeds);
  SeedParticleOriginal.Allocate(m_num_seeds);
  SeedValidity.Allocate(m_num_seeds);

  auto seed_portal = SeedParticleArray.GetPortalControl();
  auto seed_validity = SeedValidity.GetPortalControl();
  std::ifstream seed_stream(m_seed_path);
  float x1, y1, z1; 
  int seed_counter = 0;
  
  while(seed_stream >> x1)
  {
    seed_stream >> y1;
    seed_stream >> z1;
    seed_portal.Set(seed_counter, vtkm::Particle(vtkm::Vec<vtkm::FloatDefault, 3>(x1,y1,z1),seed_counter)); 
    seed_counter++;
  }

  vtkm::cont::ArrayCopy(SeedParticleArray, SeedParticleOriginal);
  auto seed_original_portal = SeedParticleOriginal.GetPortalControl();


/** 2. READING THE FIRST FILE TO IDENTIFY BBOX + SEED VALIDITY, ADJACENT NODES **/  

  std::stringstream filename;
  filename << m_input_path << "Eulerian_" << rank << "_" << m_start_cycle << ".vtk"; // Load the first basis flow file.

  vtkm::cont::DataSet dataset_info;
  vtkm::io::reader::VTKDataSetReader reader(filename.str().c_str());
  dataset_info = reader.ReadDataSet();

  vtkm::Bounds bounds = dataset_info.GetCoordinateSystem().GetBounds();
  double BB[6];
  BB[0] = bounds.X.Min;  
  BB[1] = bounds.X.Max;  
  BB[2] = bounds.Y.Min;  
  BB[3] = bounds.Y.Max;  
  BB[4] = bounds.Z.Min;  
  BB[5] = bounds.Z.Max;  
    
/** Using extracted BBox information - check seed validity and identify seeds that belong to this block **/

  for(int i = 0; i < m_num_seeds; i++)
  {
     auto pt = seed_portal.Get(i).Pos;
     if(BoundsCheck(pt[0], pt[1], pt[2], BB))
     {
       seed_validity.Set(i,1);
     }
     else
     {
       seed_validity.Set(i,0);
     }
  }

/** Sharing bbox information with all other nodes, so that adjacent nodes can be identified. **/

  double *bbox_list = (double*)malloc(sizeof(double)*6*num_ranks);
  bool allReceived[num_ranks] = {false};
  allReceived[rank] = true;

  if(num_ranks > 1)
  {
    bbox_list[rank*6 + 0] = bounds.X.Min;
    bbox_list[rank*6 + 1] = bounds.X.Max;
    bbox_list[rank*6 + 2] = bounds.Y.Min;
    bbox_list[rank*6 + 3] = bounds.Y.Max;
    bbox_list[rank*6 + 4] = bounds.Z.Min;
    bbox_list[rank*6 + 5] = bounds.Z.Max;

    for(int i = 0; i < num_ranks; i++)
    {
      if(i != rank)
      {
        int ierr = MPI_Bsend(BB, 6, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
      }
    }

    while(!AllMessagesReceived(allReceived, num_ranks))
    {
      MPI_Status probe_status, recv_status;
      int ierr = MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &probe_status);
      int count;
      MPI_Get_count(&probe_status, MPI_DOUBLE, &count);
      double *recvbuff;
      recvbuff = (double*)malloc(sizeof(double)*count);
      MPI_Recv(recvbuff, count, MPI_DOUBLE, probe_status.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &recv_status);
      if(count == 6)
      {
        bbox_list[6*probe_status.MPI_SOURCE + 0] = recvbuff[0];
        bbox_list[6*probe_status.MPI_SOURCE + 1] = recvbuff[1];
        bbox_list[6*probe_status.MPI_SOURCE + 2] = recvbuff[2];
        bbox_list[6*probe_status.MPI_SOURCE + 3] = recvbuff[3];
        bbox_list[6*probe_status.MPI_SOURCE + 4] = recvbuff[4];
        bbox_list[6*probe_status.MPI_SOURCE + 5] = recvbuff[5];

        allReceived[recv_status.MPI_SOURCE] = true;
      }
      else
      {
        std::cout << "[" << rank << "] Corrupt message received from " << probe_status.MPI_SOURCE << std::endl;
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
  } 

  vtkm::cont::DataSet start_grid, end_grid;
  int start, end;

/** 2. START LOOP OVER INTERVALS **/
  int file_load_cycle = m_start_cycle;

  for(int cycle = m_start_cycle; cycle <= m_end_cycle; cycle += 1)
  {
    if(cycle == file_load_cycle)
    {
      file_load_cycle += m_interval;
      start = cycle;
      end = file_load_cycle; 
      if(file_load_cycle > m_end_cycle)
      {
        break;
      }
      std::stringstream filename1, filename2;
      filename1 << m_input_path << "Eulerian_" << rank << "_" << cycle << ".vtk"; 
      filename2 << m_input_path << "Eulerian_" << rank << "_" << file_load_cycle << ".vtk";

      vtkm::io::reader::VTKDataSetReader reader1(filename1.str().c_str());
      start_grid = reader1.ReadDataSet();
      vtkm::io::reader::VTKDataSetReader reader2(filename2.str().c_str());
      end_grid = reader2.ReadDataSet();
    }
    
    /* Calculate the field that needs to be passed to the particle advection worklet  */
    
    using FieldHandle = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>>;
    int num_grid_pts = start_grid.GetNumberOfPoints();
    FieldHandle fieldArray;
    fieldArray.Allocate(num_grid_pts);

    FieldHandle start_field = start_grid.GetField("velocity").GetData().Cast<FieldHandle>();
    FieldHandle end_field = end_grid.GetField("velocity").GetData().Cast<FieldHandle>();
    
    if(cycle == start)
    {
      vtkm::cont::ArrayCopy(start_field, fieldArray);
    }
    else if(cycle > start && cycle < end)
    {
      // Calculate fieldArray values
      float x,y,z;
      auto start_portal = start_field.GetPortalControl(); 
      auto end_portal = end_field.GetPortalControl(); 
      auto field_portal = fieldArray.GetPortalControl(); 

      for(int i = 0; i < num_grid_pts; i++)
      {
        auto vel1 = start_portal.Get(i);
        auto vel2 = end_portal.Get(i);
        float t = ((cycle - start)*1.0)/((end - start)*1.0);
        x = vel1[0] + t*(vel2[0] - vel1[0]);
        y = vel1[1] + t*(vel2[1] - vel1[1]);
        z = vel1[2] + t*(vel2[2] - vel1[2]);
        field_portal.Set(i, vtkm::Vec<vtkm::FloatDefault, 3>(static_cast<vtkm::FloatDefault>(x), 
          static_cast<vtkm::FloatDefault>(y), static_cast<vtkm::FloatDefault>(z)));
      }
    }
    else
    { // cycle == end This case should not happen
      vtkm::cont::ArrayCopy(end_field, fieldArray);
    }
    
    /* Perform particle advection */
    const vtkm::cont::DynamicCellSet& cells = start_grid.GetCellSet();
    const vtkm::cont::CoordinateSystem& coords = start_grid.GetCoordinateSystem();

    using GridEvalType = vtkm::worklet::particleadvection::GridEvaluator<FieldHandle>;
    using RK4Type = vtkm::worklet::particleadvection::RK4Integrator<GridEvalType>;
    vtkm::worklet::ParticleAdvection particleadvection;
    vtkm::worklet::ParticleAdvectionResult res;

    GridEvalType gridEval(coords, cells, fieldArray);
    RK4Type rk4(gridEval, static_cast<vtkm::Float32>(m_step_size));

    res = particleadvection.Run(rk4, SeedParticleArray, 1); // Taking a single step

    auto seed_particles = res.Particles;
    auto seed_particles_portal = seed_particles.GetPortalControl();

    auto seed_original_portal = SeedParticleOriginal.GetPortalControl();
    auto seed_array_portal = SeedParticleArray.GetPortalControl();

    /* Communicate particles */
    
    std::vector<int> outgoing_id;
    std::vector<int> outgoing_dest;

    for(int i = 0; i < m_num_seeds; i++)
    {
      if(seed_validity.Get(i))
      {
        auto pt = seed_particles_portal.Get(i).Pos;
        if(!BoundsCheck(pt[0], pt[1], pt[2], BB))
        {
          seed_validity.Set(i, 0);
          int dest = -1;
          for(int r = 0; r < num_ranks; r++)
          {
            if(r != rank)
            {
              if(BoundsCheck(pt[0], pt[1], pt[2],
               bbox_list[r*6 + 0], bbox_list[r*6 + 1], bbox_list[r*6 + 2], bbox_list[r*6 + 3], bbox_list[r*6 + 4], bbox_list[r*6 + 5]))
              {
                dest = r;
                break;
              }
            }
          }
          if(dest != -1)
          {
            outgoing_id.push_back(i);
            outgoing_dest.push_back(dest);
          }
        }
      }
    }

    if(num_ranks > 1)
    {
      MPI_Barrier(MPI_COMM_WORLD);

      int bufsize = (m_num_seeds*MSG_LENGTH + (MPI_BSEND_OVERHEAD * num_ranks)); // message packet size 4 + message packet size 1 for empty sends.
      double *buf = (double*)malloc(sizeof(double)*bufsize);
      MPI_Buffer_attach(buf, bufsize);
      // Sending messages to all ranks.
      // Message format: ID X Y Z ID X Y Z ..
      // If not particles to send: -1 
      for(int r = 0; r < num_ranks; r++)
      {
        if(r != rank)
        {
          int particles_to_send = 0;
          std::vector<double> message;
          for(int j = 0; j < outgoing_dest.size(); j++)
          {
            if(outgoing_dest[j] == r)
            {
              auto pt1 = seed_particles_portal.Get(outgoing_id[j]).Pos;
              auto pt2 = seed_original_portal.Get(outgoing_id[j]).Pos;
              particles_to_send++;
              message.push_back(outgoing_id[j]);
              message.push_back(pt1[0]);
              message.push_back(pt1[1]);
              message.push_back(pt1[2]);
              message.push_back(pt2[0]);
              message.push_back(pt2[1]);
              message.push_back(pt2[2]);
            }
          }
          int buffsize = particles_to_send*MSG_LENGTH;
          double *sendbuff;

          if(buffsize == 0)
            sendbuff = (double*)malloc(sizeof(double));
          else
            sendbuff = (double*)malloc(sizeof(double)*buffsize);

          if(buffsize  == 0)
          {
            buffsize = 1;
            sendbuff[0] = -1.0;
          }
          else
          {
            for(int k = 0; k < message.size(); k++)
            {
              sendbuff[k] = message[k];
            }
          }
          int ierr = MPI_Bsend(sendbuff, buffsize, MPI_DOUBLE, r, 13, MPI_COMM_WORLD);
          free(sendbuff);
        }
      }   // Loop over all ranks.
      MPI_Barrier(MPI_COMM_WORLD);

      // Receive Messages
      bool allReceived2[num_ranks] = {false};
      allReceived2[rank] = true;

      while(!AllMessagesReceived(allReceived2, num_ranks))
      {
        MPI_Status probe_status, recv_status;
        int ierr = MPI_Probe(MPI_ANY_SOURCE, 13, MPI_COMM_WORLD, &probe_status);
        int count;
        MPI_Get_count(&probe_status, MPI_DOUBLE, &count);
        double *recvbuff;
        recvbuff = (double*)malloc(sizeof(double)*count);
        MPI_Recv(recvbuff, count, MPI_DOUBLE, probe_status.MPI_SOURCE, 13, MPI_COMM_WORLD, &recv_status);
        if(count == 1)
        {
          allReceived2[recv_status.MPI_SOURCE] = true;
        }
        else if(count % MSG_LENGTH == 0)
        {
          int num_particles = count/MSG_LENGTH;

          for(int i = 0; i < num_particles; i++)
          {
            int index = recvbuff[i*MSG_LENGTH+0];
            seed_validity.Set(index, 1);
            seed_array_portal.Set(index, vtkm::Particle(vtkm::Vec<vtkm::FloatDefault, 3>
            (recvbuff[i*MSG_LENGTH+1], recvbuff[i*MSG_LENGTH+2], recvbuff[i*MSG_LENGTH+3]), index));
            seed_original_portal.Set(index, vtkm::Particle(vtkm::Vec<vtkm::FloatDefault, 3>
            (recvbuff[i*MSG_LENGTH+4], recvbuff[i*MSG_LENGTH+5], recvbuff[i*MSG_LENGTH+6]), index));
          }
          allReceived2[recv_status.MPI_SOURCE] = true;
        }
        else
        {
          std::cout << "[" << rank << "] Received message of invalid length from : " << recv_status.MPI_SOURCE << std::endl;
          allReceived2[recv_status.MPI_SOURCE] = true;
        }
        free(recvbuff);
      }
      MPI_Buffer_detach(&buf, &bufsize);
      free(buf);
      MPI_Barrier(MPI_COMM_WORLD);
    } // endif rank > 1

    /*  Write Output Start  */

    if(cycle % m_interval == 0)
    {
      int connectivity_index = 0;
      std::vector<vtkm::Id> connectivity;
      std::vector<vtkm::Vec<vtkm::Float64, 3>> pointCoordinates;
      std::vector<vtkm::UInt8> shapes;
      std::vector<vtkm::IdComponent> numIndices;
      std::vector<vtkm::Id> pathlineId;

      for(int i = 0; i < m_num_seeds; i++)
      {
        if(seed_validity.Get(i))
        {
          auto pt1 = seed_original_portal.Get(i).Pos;
          auto pt2 = seed_array_portal.Get(i).Pos;

          connectivity.push_back(connectivity_index);
          connectivity.push_back(connectivity_index + 1);
          connectivity_index += 2;
          pointCoordinates.push_back(
             vtkm::Vec<vtkm::Float64, 3>(pt1[0], pt1[1], pt1[2]));
          pointCoordinates.push_back(
             vtkm::Vec<vtkm::Float64, 3>(pt2[0], pt2[1], pt2[2]));
          shapes.push_back(vtkm::CELL_SHAPE_LINE);
          numIndices.push_back(2);
          pathlineId.push_back(i);
        }
      }

      vtkm::cont::DataSetBuilderExplicit DSB_Explicit;
      vtkm::cont::DataSet pathlines = DSB_Explicit.Create(pointCoordinates, shapes, numIndices, connectivity);
      vtkm::cont::DataSetFieldAdd dsfa;
      dsfa.AddCellField(pathlines, "ID", pathlineId);

      std::stringstream outputfile;
      outputfile << m_output_path << "EulerianPathlines_" << rank << "_" << cycle << ".vtk";

      vtkm::io::writer::VTKDataSetWriter writer(outputfile.str().c_str());
      writer.WriteDataSet(pathlines);

      vtkm::cont::ArrayCopy(SeedParticleArray, SeedParticleOriginal);
    }
  } // Cycle loop
    
  this->m_output = new DataSet();
  const int num_domains = this->m_input->GetNumberOfDomains();
  vtkm::Id domain_id;
  vtkm::cont::DataSet dom;
  this->m_input->GetDomain(0, dom, domain_id);
//    if(dom.HasField(m_field_name))
//    {
//      using vectorField_d = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>>;
//      using vectorField_f = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3>>;
//      auto field = dom.GetField(m_field_name).GetData();
//      if(!field.IsType<vectorField_d>() && !field.IsType<vectorField_f>())
//      {
//        throw Error("Vector field type does not match <vtkm::Vec<vtkm::Float32,3>> or <vtkm::Vec<vtkm::Float64,3>>");
//      }
//    }
//    else
//    {
//      throw Error("Domain does not contain specified vector field for Eulerian analysis.");
//    }
//
  m_output->AddDomain(dom, domain_id);
  
  #endif
}

inline bool EulerianInterpolation::BoundsCheck(float x, float y, float z, double *BB)
{ 
  if(x >= BB[0] && x <= BB[1] && y >= BB[2] && y <= BB[3] && z >= BB[4] && z <= BB[5])
  { 
    return true;
  }
  return false;
}

inline bool EulerianInterpolation::BoundsCheck(float x, float y, float z, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
{ 
  if(x >= xmin && x <= xmax && y >= ymin && y <= ymax && z >= zmin && z <= zmax)
  { 
    return true;
  }
  return false;
}


inline bool EulerianInterpolation::AllMessagesReceived(bool *a, int num_ranks)
{
for(int i = 0; i < num_ranks; i++)
{ 
  if(a[i] == false)
    return false;
}
return true;
}

std::string
EulerianInterpolation::GetName() const
{
  return "vtkh::EulerianInterpolation";
}

} //  namespace vtkh
