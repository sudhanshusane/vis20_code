#ifndef VTK_H_EULERIANINTERPOLATION_HPP
#define VTK_H_EULERIANINTERPOLATION_HPP

#include <vtkh/vtkh_exports.h>
#include <vtkh/vtkh.hpp>
#include <vtkh/filters/Filter.hpp>
#include <vtkh/DataSet.hpp>

namespace vtkh
{

class VTKH_API EulerianInterpolation : public Filter
{
public:
  EulerianInterpolation();
  virtual ~EulerianInterpolation();
  std::string GetName() const override;
	void SetField(const std::string &field_name);
	void SetSeedPath(const std::string &seed_path);
	void SetOutputPath(const std::string &output_path);
	void SetInputPath(const std::string &input_path);
	void SetNumSeeds(const int &num_seeds);
	void SetInterval(const int &interval);
	void SetStartCycle(const int &start_cycle);
	void SetEndCycle(const int &end_cycle);
  void SetStepSize(const double &step_size);

private:
  bool AllMessagesReceived(bool *a, int num_ranks);
  bool BoundsCheck(float x, float y, float z, double *BB);
  bool BoundsCheck(float x, float y, float z, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

protected:
  void PreExecute() override;
  void PostExecute() override;
  void DoExecute() override;

  std::string m_field_name;
  std::string m_seed_path;
  std::string m_output_path;
  std::string m_input_path;
  int m_num_seeds;
  int m_interval;
  int m_start_cycle;
  int m_end_cycle;
	double m_step_size;
};

} //namespace vtkh
#endif
