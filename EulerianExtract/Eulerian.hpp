#ifndef VTK_H_EULERIAN_HPP
#define VTK_H_EULERIAN_HPP

#include <vtkh/vtkh_exports.h>
#include <vtkh/vtkh.hpp>
#include <vtkh/filters/Filter.hpp>
#include <vtkh/DataSet.hpp>

namespace vtkh
{

class VTKH_API Eulerian : public Filter
{
public:
  Eulerian();
  virtual ~Eulerian();
  std::string GetName() const override;
	void SetField(const std::string &field_name);
	void SetOutputPath(const std::string &output_path);
  void SetWriteFrequency(const int &write_frequency);
  void SetDomainId(const int &domain_id);

protected:
  void PreExecute() override;
  void PostExecute() override;
  void DoExecute() override;

  std::string m_field_name;
  std::string m_output_path;
	int m_write_frequency;
  int m_domain_id;
  
};

} //namespace vtkh
#endif
