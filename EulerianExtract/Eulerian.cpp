#include <iostream>
#include <vtkh/filters/Eulerian.hpp>
#include <vtkh/vtkh.hpp>
#include <vtkh/Error.hpp>
#include <vtkh/DataSet.hpp>
#include <vtkh/filters/GhostStripper.hpp>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/cont/DataSetBuilderRectilinear.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/VariantArrayHandle.h>
#include <vtkm/cont/ArrayHandleVirtual.h>
#include <vtkm/Types.h>


static vtkm::Id cycle = 0;

namespace vtkh
{

Eulerian::Eulerian()
{
}

Eulerian::~Eulerian()
{
}

void
Eulerian::SetField(const std::string &field_name)
{
  m_field_name = field_name;
}

void
Eulerian::SetOutputPath(const std::string &output_path)
{
  m_output_path = output_path;
}

void
Eulerian::SetWriteFrequency(const int &write_frequency)
{
  m_write_frequency = write_frequency;
}

void
Eulerian::SetDomainId(const int &domain_id)
{
  m_domain_id = domain_id;
}

void Eulerian::PreExecute()
{
  Filter::PreExecute();
  Filter::CheckForRequiredField(m_field_name);
}

void Eulerian::PostExecute()
{
  Filter::PostExecute();
}

void Eulerian::DoExecute()
{
  vtkm::Id rank = vtkh::GetMPIRank();
  vtkm::Id dom_id;
  vtkm::cont::DataSet dom;
  int m_strip_ghost_cells = 1;
  if(m_strip_ghost_cells)
  {
    vtkh::GhostStripper stripper;
    stripper.SetInput(this->m_input);
    stripper.SetField("ascent_ghosts");
    stripper.Update();
    vtkh::DataSet *min_dom = stripper.GetOutput();
    min_dom->GetDomain(m_domain_id, dom, dom_id);
  }
  else
  { 
    this->m_input->GetDomain(m_domain_id, dom, dom_id);
  }
  
  if(dom.HasField(m_field_name))
  {
    using vectorField_d = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>>;
    using vectorField_f = vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3>>;
    vtkm::cont::VariantArrayHandle field = dom.GetField(m_field_name).GetData();
    
    if(!field.IsType<vectorField_d>() && !field.IsType<vectorField_f>())
    {
      throw Error("Vector field type does not match <vtkm::Vec<vtkm::Float32,3>> or <vtkm::Vec<vtkm::Float64,3>>");
    }
    vtkm::cont::ArrayHandleVirtual<vtkm::Vec<vtkm::Float64,3>> v_FieldArray =  field.AsVirtual<vtkm::Vec<vtkm::Float64,3>>();
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>> fieldArray = v_FieldArray.Cast<vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float64, 3>>>();

    if(cycle % m_write_frequency == 0)
    {
      using AxisHandle = vtkm::cont::ArrayHandle<vtkm::FloatDefault>;               
      using AxisPortalType = typename AxisHandle::PortalConstControl;                      
                                                                                    
      using RectilinearType = vtkm::cont::ArrayHandleCartesianProduct<AxisHandle, AxisHandle, AxisHandle>;
      using RectilinearPortalType = typename RectilinearType::PortalConstControl;          
                                                                                    
      RectilinearType coords = dom.GetCoordinateSystem().GetData().template Cast<RectilinearType>();
      RectilinearPortalType coords_portal = coords.GetPortalConstControl();                     
      AxisPortalType xCoords = coords_portal.GetFirstPortal();                             
      AxisPortalType yCoords = coords_portal.GetSecondPortal();                            
      AxisPortalType zCoords = coords_portal.GetThirdPortal(); 
      std::vector<vtkm::Float64> xC, yC, zC;

      for(int i = 0 ; i < xCoords.GetNumberOfValues(); i++)
      {
        xC.push_back(xCoords.Get(i));
      }
      for(int i = 0 ; i < yCoords.GetNumberOfValues(); i++)
      {
        yC.push_back(yCoords.Get(i));
      }
      for(int i = 0 ; i < zCoords.GetNumberOfValues(); i++)
      {
        zC.push_back(zCoords.Get(i));
      }
    
      vtkm::cont::DataSet outputData;
      vtkm::cont::DataSetBuilderRectilinear dataSetBuilder;
      outputData = dataSetBuilder.Create(xC, yC, zC);
      vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
      dataSetFieldAdd.AddPointField(outputData, m_field_name, fieldArray);
      std::stringstream file_stream;
      file_stream << m_output_path << "Eulerian_" << rank << "_" << cycle << ".vtk";
      vtkm::io::writer::VTKDataSetWriter writer(file_stream.str());
      writer.WriteDataSet(outputData);
    }
  }
/*  else if()
  { // In case you want to derive the velocity field from other fields (for example, momentum/density) 
  } */ 
  else
  {
    throw Error("Domain does not contain specified vector field for Eulerian analysis.");
  }
  cycle += 1;
  this->m_output = new DataSet();
  this->m_output->AddDomain(dom, dom_id);
}

std::string
Eulerian::GetName() const
{
  return "vtkh::Eulerian";
}

} //  namespace vtkh
