#ifndef VTK_H_PATHLINES_HPP
#define VTK_H_PATHLINES_HPP

#include <vtkh/vtkh_exports.h>
#include <vtkh/vtkh.hpp>
#include <vtkh/filters/Filter.hpp>
#include <vtkh/DataSet.hpp>

namespace vtkh
{

class VTKH_API Pathlines : public Filter
{
public:
  Pathlines();
  virtual ~Pathlines();
  std::string GetName() const override;
	void SetField(const std::string &field_name);
	void SetSeedPath(const std::string &seed_path);
	void SetOutputPath(const std::string &output_path);
	void SetStepSize(const double &step_size);
	void SetNumSeeds(const int &num_seeds);
	void SetInterval(const int &interval);
  void SetDomainId(const int &domain_id);

private:
  bool AllMessagesReceived(bool *a, int num_ranks);
  bool BoundsCheck(float x, float y, float z, double *BB);
  bool BoundsCheck(float x, float y, float z, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);
  void InitializeSeedArrays();

protected:
  void PreExecute() override;
  void PostExecute() override;
  void DoExecute() override;

  std::string m_field_name;
	double m_step_size;
  std::string m_seed_path;
  std::string m_output_path;
  int m_num_seeds;
  int m_interval;
  int m_domain_id;


};

} //namespace vtkh
#endif
